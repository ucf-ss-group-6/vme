#!/bin/bash
if [ ! -d "bin" ]; then
  mkdir bin;
fi
res=$(gcc src/vm.c -o bin/vm && echo "build success; running with args: $@" && chmod +x bin/vm)
exit $?
