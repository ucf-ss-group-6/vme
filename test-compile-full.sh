#!/bin/bash
set -e
set -o pipefail

echo "compiling compiler"
make compile bin/vm

plcode='doc/full-compiler/demo.pl0'
pmcode='demo.pm0'

echo "compiling demo.pl0"
./compile $plcode $pmcode

echo "running demo.pm0"
bin/vm $pmcode 2>&1 | grep output | tee log.txt
echo "it should output 7, 9, 11"

echo "testing against course repo test case"
refPath="submodule/course_repo/3_compile_full"
ls "$refPath" | grep -v vm | grep -v '\.m' | grep -v '\.txt' | xargs -I {} echo echo "--------" \; echo "compiling {}"\; ./compile "$refPath/{}" "{}.pm0"\; echo "comparing {}.pm0 with $refPath/{}.m" \; diff "{}.pm0" "$refPath/{}.m" | source /dev/stdin

#manually confirmed by Beeno
#echo "testing against our last tiny-PL/0 test case"
#refPath="doc/parser/correct_cases"
#ls "$refPath" | grep "\.pl0" | sed 's/\.pl0//' | xargs -I {} echo echo "--------" \; echo "compiling {}"\; ./compile "$refPath/{}.pl0" "{}.pm0"\; echo "comparing {}.pm0 with $refPath/{}.pm0" \; diff "{}.pm0" "$refPath/{}.pm0" | source /dev/stdin
