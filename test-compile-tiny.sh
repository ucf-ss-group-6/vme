#!/bin/bash
#make bin/parser && bin/parser < submodule/extra-test-case/res/lexer/output/basic.beenotung.token
set -e
set -o pipefail

make

mkdir -p output

## standard test case
echo "--------------------------------"
echo "testing against standard test cases in course repo ..."
ls submodule/course_repo/2_parser/a_supporting_files/*.pl0 | awk -F '/' '{print $NF}' | sed 's/\.pl0$//' | xargs -I {} echo echo "" \&\& echo "compiling {}.pm0 ..." \&\& bin/compile-tiny submodule/course_repo/2_parser/a_supporting_files/{}.pl0 output/{}.pm0  \| tail -n -1 \&\& echo diff on {}.pm0: \&\& diff submodule/course_repo/2_parser/a_supporting_files/{}.pm0 output/{}.pm0 \&\& echo "test-{} : passed" | source /dev/stdin

echo ""

## extra test cases
echo "--------------------------------"
echo "testing against extra cases ..."
has_failed=0

echo ""
echo "compiling fib.pl0 ..."
bin/compile-tiny submodule/extra-test-case/res/parser/fib.pl0 output/fib.pm0 | tail -n -1
echo "running fib.pm0 ..."
res=$(echo 1548008755920 | bin/vm output/fib.pm0 | grep '^output:' | tail -n 1 | sed 's/output: //')
expect=1134903170
if [ $res -eq $expect ]; then
  echo "test-fib : passed";
else
  echo "test-fib : failed";
  echo "expect: $expect, seen: $res"
  has_failed=1
fi

echo ""
echo "compiling prime.pl0 ..."
bin/compile-tiny submodule/extra-test-case/res/parser/prime.pl0 output/prime.pm0 | tail -n -1
echo "running prime.pm0 ..."
res=$(echo 1024 | bin/vm output/prime.pm0 | grep '^output:' | wc -l)
expect=172
if [ $res -eq $expect ]; then
  echo "test-prime : passed";
else
  echo "test-prime : failed";
  echo "expect: $expect, seen: $res"
  has_failed=1
fi

echo ""
echo "Finished all test."
if [ $has_failed -ne 0 ]; then
  echo "Failed on some extra test cases"
else
  echo "Passed all extra test cases"
fi

rm -rf output
