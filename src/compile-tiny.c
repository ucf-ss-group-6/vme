#include <stdio.h>
#include <stdlib.h>
#include "utils.c"
#include <string.h>
#include <unistd.h>

struct lines_s {
    char *line;
    int n;
    struct lines_s *next;
};

int run_command(char *cmd, struct lines_s *res_head) {
    //    printf("run command: %s\n", cmd);
    FILE *fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    fp = popen(cmd, "r");
    if (fp == NULL) {
        printf("\nError: Failed to run command: %s\n", cmd);
        return EXIT_FAILURE;
    }
    while ((read = getline(&line, &len, fp)) != EOF) {
        res_head->next = checked_malloc(sizeof(struct lines_s), "new record for struct lines_s (run_command)");
        res_head = res_head->next;
        res_head->next = NULL;
        res_head->line = clone_string(line);
        res_head->n = read;
    }
    return pclose(fp);
}

bool has_error(struct lines_s *head) {
    for (head = head->next; head != NULL; head = head->next) {
        if (strncmp("Error: ", head->line, 7) == 0) {
            return true;
        }
    }
    return false;
}

void print_all_lines(struct lines_s *head) {
    for (head = head->next; head != NULL; head = head->next) {
        printf("%s", head->line);
    }
}

void save_all_lines(struct lines_s *head, string filename) {
    FILE *fp = fopen(filename, "w");
    if (fp == NULL) {
        printf("\nError: Failed to open file: %s\n", filename);
        exit(EXIT_FAILURE);
    }
    for (head = head->next; head != NULL; head = head->next) {
        fprintf(fp, "%s", head->line);
    }
    fclose(fp);
}

int main(int argc, char **args) {
    if (argc != 1 + 2) {
        printf("\nError: expect two arguments\n");
        printf("example: %s code.pl0 code.pm0\n", args[0]);
        exit(EXIT_FAILURE);
    }

    char *plcode_filename = args[1];
    char *pmcode_filename = args[2];
    char token_filename[] = "/tmp/token-XXXXXXXX";
    int res;
    int i;
    char *cmd;
    char *cmd_tmp;
    int len_cmd;
    struct lines_s *lexer_res = checked_malloc(sizeof(struct lines_s), "lexer_res");
    struct lines_s *parser_res = checked_malloc(sizeof(struct lines_s), "parser_res");

    lexer_res->next = NULL;
    parser_res->next = NULL;

    check_and_fix_newline(plcode_filename);

    cmd_tmp = "bin/lexer %s | tail -n +4";
    len_cmd = strlen(cmd_tmp) - 2 + strlen(plcode_filename) + 1;
    cmd = checked_malloc(sizeof(char) * len_cmd, "cmd");
    sprintf(cmd, cmd_tmp, plcode_filename);
    cmd[len_cmd - 1] = '\0';
    res = run_command(cmd, lexer_res);
    if (res != EXIT_SUCCESS || has_error(lexer_res)) {
        print_all_lines(lexer_res);
        exit(res == EXIT_SUCCESS ? EXIT_FAILURE : res);
    }

    i = mkstemp(token_filename);
    if (i == -1) {
        printf("\nError: failed to create temp file for tokens\n");
        return EXIT_FAILURE;
    }

    save_all_lines(lexer_res, token_filename);

    //    free(cmd_tmp);
    cmd_tmp = "bin/parser %s %s";
    len_cmd = strlen(cmd_tmp) - 2 * 2 + strlen(token_filename) + strlen(pmcode_filename) + 1;
    cmd = checked_realloc(cmd, sizeof(char) * len_cmd, "cmd");
    sprintf(cmd, cmd_tmp, token_filename, pmcode_filename);
    cmd[len_cmd - 1] = '\0';
    res = run_command(cmd, parser_res);
    if (res != EXIT_SUCCESS || has_error(parser_res)) {
        print_all_lines(parser_res);
        exit(res == EXIT_SUCCESS ? EXIT_FAILURE : res);
    } else {
        unlink(token_filename);
    }

    print_all_lines(parser_res);

    return EXIT_SUCCESS;
}
