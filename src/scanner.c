/*
 * bundled compiler, do lexer, parsing and code_gen at once
 * */

#include <stdlib.h>
#include <stdio.h>

#include "utils.c"

#undef exit
#define exit fflush(stdout);exit


int line = 1;
int col = 0;
int last_col = 0;

FILE *fp_source_code;

/**
 * @remark cannot fall back more than one line
 * */
void unGetChar(char c) {
    ungetc(c, fp_source_code);
    if (c == '\n') {
        line--;
        col = last_col;
    } else {
        col--;
    }
}

/**
 * will not skip comment
 * */
char getChar_raw() {
    last_col = col;
    char c = fgetc(fp_source_code);
    if (c == '\n') {
        line++;
        col = 1;
    } else {
        col++;
    }
    return c;
}

char getChar_raw_old() {
    char c;
    last_col = col;
    for (;;) {
        col++;
        c = fgetc(fp_source_code);
        if (c == '\n') {
            line++;
            col = 1;
        } else {
            break;
        }
    }
    return c;
}

/**
 * will skip comment
 * */
char getChar() {
    char c1 = getChar_raw();
    if (c1 != '/') {
        return c1;
    }
    char c2 = getChar_raw();
    if (c2 != '*') {
        unGetChar(c2);
        return c1;
    }
    char c3;
L1:
    for (;;) {
        c3 = getChar_raw();
        if (c3 == '*') {
            break;
        }
        if (c3 == EOF) {
            fflush(stdout);
            fflush(stderr);
            fprintf(stderr, "Error: comment not closed!\n");
            exit(1);
        }
    }
    char c4 = getChar_raw();
    if (c4 == '/') {
        unGetChar(' ');
        return getChar_raw();
    } else {
        goto L1;
    }
}


/**
 * @remark resulting buffer's whitespace might not align with source code (will mess up line and col)
 * */
void unGetWord(string s) {
    int n = strlen(s);
    int i;
    for (i = n - 1; i >= 0; i--) {
        unGetChar(s[i]);
    }
}

void unGetNumber(int value) {
    bool is_negative = false;
    if (value == 0) {
        unGetChar('0');
        return;
    }
    if (value < 0) {
        is_negative = true;
        value = -value;
    }
    for (;;) {
        if (value == 0) {
            break;
        }
        unGetChar('0' + (value % 10));
        value /= 10;
    }
    if (is_negative) {
        unGetChar('-');
    }
}


/**
 * @remark won't stop if the char is not exist
 * */
void skip_char_until(char c) {
    char res;
    for (res = getChar(); res != c; res = getChar());
    unGetChar(res);
}

/**
 * @remark won't stop if the char is not exist
 * */
void skip_char_include(char c) {
    char res;
    for (res = getChar(); res != c; res = getChar());
}

/**
 * skip whitespace, expect EOF
 * TODO skip comment as well
 * @return bool : always return true for chained op
 * TODO return consumed string for rollback
 * */
bool skip_whitespace() {
    char res;
    for (;;) {
        res = getChar();
        if (res == EOF) {
            unGetChar(EOF);
            break;
        }
        if (!is_whitespace(res)) {
            unGetChar(res);
            break;
        }
    }
    return true;
}

bool eat_whitespace() {
    char c = getChar();
    bool res;
    if (!(res = is_whitespace(c))) {
        unGetChar(c);
    }
    return res;
}


int main(int argc, char **args) {
    if (argc != 1 + 1) {
        fflush(stdout);
        fprintf(stderr, "\nError: invalid arguments\n");
        fprintf(stderr, "example: %s code.pl0\n", args[0]);
        exit(EXIT_FAILURE);
    }
    string plcode_filename = args[1];

    fp_source_code = fopen(plcode_filename, "r");
    if (fp_source_code == NULL) {
        fflush(stdout);
        fprintf(stderr, "\nError: failed to open source code file: %s\n", plcode_filename);
        exit(1);
    }

    /* ---- main body ---- */
    char c;
    for (;;) {
        c = getChar();
        if (c == EOF) {
            break;
        }
        printf("%c", c);
    }

    printf("<EOF>\n");

    fclose(fp_source_code);

    fflush(stdout);
    fflush(stderr);
    return EXIT_SUCCESS;
}