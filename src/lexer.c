// Team name: Compiler Builder 6
// Tung Cheung Leong (Beeno Tung)
// Trevor Jenkins (Snake)
// Austin VanDyne
// Joshua Yandell
// <>

#define _XOPEN_SOURCE 700

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h> // for ssize_t on ubuntu
#include <string.h>

#define bool byte
#define true 1
#define false 0

#define exit fflush(stdout);exit

//#define __debug__
#ifdef __debug__
#define printf(...) printf(__VA_ARGS__);fflush(stdout)
#endif

typedef unsigned char byte;

void *checked_malloc(size_t size, const char *name) {
    void *res = malloc(size);
    if (res == NULL) {
        printf("\nError: failed to alloc for %s\n", name);
        exit(EXIT_FAILURE);
    }
    return res;
}

void *checked_realloc(void *head, size_t size, const char *name) {
    void *res = realloc(head, size);
    if (res == NULL) {
        printf("\nError: failed to realloc for %s\n", name);
        exit(EXIT_FAILURE);
    }
    return res;
}

typedef enum {
    nulsym = 1, identsym, numbersym, plussym, minussym,
    multsym, slashsym, oddsym, eqsym, neqsym, lessym, leqsym,
    gtrsym, geqsym, lparentsym, rparentsym, commasym, semicolonsym,
    periodsym, becomessym, beginsym, endsym, ifsym, thensym,
    whilesym, dosym, callsym, constsym, varsym, procsym, writesym,
    readsym, elsesym
} token_type;
const token_type EMPTY_TOKEN = 0; /* init status will yield to this pseudo-token */
const token_type UNKNOWN_TOKEN_TYPE = -1;
/* these are not totally invalid token, they are incomplete token  */
const token_type PENDING_TOKEN = -2;
const token_type PENDING_NUMBER = -3;
const token_type PENDING_IDENT = -4;

const int MAX_IDENTIFIER_LENGTH = 12;
const int MAX_NUMBER_VALUE = 65535; /* 2^16 - 1 */

struct path_s {
    int dest_state;
    char expected_char;
    token_type token_type;
};

struct path_list_s {
    struct path_list_s *next;
    struct path_list_s *last; /* not used by non-head */
    struct path_s *data; /* not used by head */
};

/* create empty element, add to list, return pointer to the new element */
void path_list_new_path(struct path_list_s *head) {
    head->last->next = checked_malloc(sizeof(struct path_list_s), "new path node for path_list");
    head->last = head->last->next;
    head->last->next = NULL;
    head->last->data = checked_malloc(sizeof(struct path_s), "new path for path_list");
}

/**
 * logical map [state->LinkedList[rule_s]]
 *   index : state
 *   elem  : head of path_list_s
 * */
struct path_list_s *rule_table;
//int n_path_list = 0;
int n_state = 0;
int STATE_IDENT;


/*---------------------begin of init------------------------------*/
/* declare functions */
struct path_list_s *get_path_list(int state) {
    if (state >= n_state) {
        n_state++;
        /* this is new state, create new list */
        rule_table = checked_realloc(rule_table, sizeof(struct path_list_s) * n_state, "rule_table");
        /* init new path_list */ {
            rule_table[n_state - 1].next = NULL;
            rule_table[n_state - 1].last = &rule_table[n_state - 1];
        }
    }
    return &rule_table[state];
}

/**@remark using get_path_list internally */
struct path_s *get_path(int last_state, int current_char) {
    struct path_list_s *pre_path;
    for (pre_path = get_path_list(last_state); pre_path->next != NULL; pre_path = pre_path->next) {
        if (pre_path->next->data->expected_char == current_char) {
            /* found */
            return pre_path->next->data;
        }
    }
    /* not found */
    return NULL;
}

/**@remark using get_path internall */
int get_next_state(int last_state, int current_char) {
    struct path_s *path = get_path(last_state, current_char);
    return path == NULL ? -1 : path->dest_state;
}

/**@remark using get_path internall */
token_type get_next_token_type(int last_state, int current_char) {
    struct path_s *path = get_path(last_state, current_char);
    return path == NULL ? UNKNOWN_TOKEN_TYPE : path->token_type;
}

/**@remark using get_path_list internall */
void add_path(int last_state, char expected_char, int next_state, token_type tokenType) {
    struct path_list_s *path_list = get_path_list(last_state);
    path_list_new_path(path_list);
    path_list->last->data->expected_char = expected_char;
    path_list->last->data->token_type = tokenType;
    path_list->last->data->dest_state = next_state;
}

void add_string_token(char *cs, token_type tokenType) {
    int i;
    int last_state = 0;
    int next_state;
    token_type next_token;
    for (i = 0; cs[i] != '\0'; i++) {
        if ((next_state = get_next_state(last_state, cs[i])) == -1) {
            /* this is new rule, create and store it */
            /* create new state */
            next_state = n_state;
            add_path(last_state, cs[i], next_state, PENDING_TOKEN);
        } else {
            /* this is not new rule, check for conflict */
            next_token = get_next_token_type(last_state, cs[i]);
            if (next_token >= nulsym && next_token <= elsesym) {
                printf("\nError: exists conflicting rule (1): next_state = %d, next_token = %d\n",
                       next_state, next_token);
                printf("expected_c = %c, current_c = %c\n", get_path(last_state, cs[i])->expected_char, cs[i]);
                exit(EXIT_FAILURE);
            }
        }
        last_state = next_state;
    }
    /* create a terminating state to confirm the token, check for conflict, skip if duplicated */
    next_state = 0;
    next_token = get_next_token_type(last_state, ' ');
    //    if (!(next_token == UNKNOWN_TOKEN_TYPE || next_token == PENDING_TOKEN || next_token == tokenType)) {
    if (next_token >= nulsym && next_token <= elsesym) {
        printf("\nError: exists conflicting rule (2): next_state = %d, next_token = %d, new next_token = %d\n",
               next_state, next_token, tokenType);
        exit(EXIT_FAILURE);
    }
    if (next_token != tokenType) {
        add_path(last_state, ' ', next_state, tokenType);
    }
}

void init() {
    /* declare vars */
    char c;
    int current_state;

    /* init vars */
    rule_table = checked_malloc(sizeof(struct path_list_s), "rule_table");

    /* ---- main logic ---- */

    /* generate rules for reserved words */
    add_string_token("const", constsym);
    add_string_token("var", varsym);
    add_string_token("procedure", procsym);
    add_string_token("call", callsym);
    add_string_token("begin", beginsym);
    add_string_token("end", endsym);
    add_string_token("if", ifsym);
    add_string_token("then", thensym);
    add_string_token("else", elsesym);
    add_string_token("while", whilesym);
    add_string_token("do", dosym);
    add_string_token("read", readsym);
    add_string_token("write", writesym);
    add_string_token("odd", oddsym);

    /* generate rules for operators and special symbols */
    add_string_token("+", plussym);
    add_string_token("-", minussym);
    add_string_token("*", multsym);
    add_string_token("/", slashsym);
    add_string_token("=", eqsym);
    add_string_token("<>", neqsym);
    add_string_token("<=", leqsym);
    add_string_token("<", lessym);
    add_string_token(">=", geqsym);
    add_string_token(">", gtrsym);
    add_string_token(":=", becomessym);
    add_string_token(",", commasym);
    add_string_token(";", semicolonsym);
    add_string_token(".", periodsym);
    add_string_token("(", lparentsym);
    add_string_token(")", rparentsym);

    /* --- generate rules for identifier --- */
    /* allocate state for pending_ident */
    current_state = n_state;
    STATE_IDENT = current_state;
    /** @remark ```n_state++;``` will be run automatically */
    for (c = 'a'; c <= 'z'; c++) {
        add_path(0, c, current_state, PENDING_IDENT);
        add_path(current_state, c, current_state, PENDING_IDENT);
    }
    for (c = 'A'; c <= 'Z'; c++) {
        add_path(0, c, current_state, PENDING_IDENT);
        add_path(current_state, c, current_state, PENDING_IDENT);
    }
    for (c = '0'; c <= '9'; c++) {
        add_path(current_state, c, current_state, PENDING_IDENT);
    }
    add_path(current_state, ' ', 0, identsym);

    /* --- generate rules for number --- */
    /* allocate state for pending_number */
    current_state = n_state;
    /** @remark ```n_state++;``` will be run automatically */
    for (c = '0'; c <= '9'; c++) {
        add_path(0, c, current_state, PENDING_NUMBER);
        add_path(current_state, c, current_state, PENDING_NUMBER);
    }
    add_path(current_state, ' ', 0, numbersym);

    /* --- generate rule for continuous whitespace */
    add_path(0, ' ', 0, EMPTY_TOKEN);

#ifdef __debug__
    //    /* print out result (debug) */
    //    int i_state;
    //    struct path_list_s *i_rule;
    //    for (i_state = 0; i_state < n_state; i_state++) {
    //        for (i_rule = &rule_table[i_state]; i_rule->next != NULL; i_rule = i_rule->next) {
    //            printf("state: %2d -> %2d, expected_char: '%c', token: %2d\n", i_state, i_rule->next->data->dest_state,
    //                   i_rule->next->data->expected_char, i_rule->next->data->token_type);
    //        }
    //    }
#endif
}
/*---------------------end of init------------------------------*/

bool is_visible_char(int c) {
    return c >= 33 && c <= 126 ? true : false;
}

bool is_digit(int c) {
    return c >= '0' && c <= '9' ? true : false;
}

/**
 * @return zero / positive int : no error
 * @return -1 : not integer
 * @return -2 : excess @invariant MAX_NUMBER_VALUE
 * */
int number_value(char *cs, int n) {
    int i;
    int value = 0;
    for (i = 0; i < n; i++) {
        if (is_digit(cs[i])) {
            value = value * 10 + (cs[i] - '0');
            if (value > MAX_NUMBER_VALUE) {
                return -2;
            } else {
                continue;
            }
        } else {
            return -1;
        }
    }
    return value;
}

bool is_alphabet(int c) {
    return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') ? true : false;
}

bool is_symbol(int c) {
    return is_visible_char(c) && !is_digit(c) && !is_alphabet(c) ? true : false;
}

bool is_whitespace(int c) {
    switch (c) {
    case '\r':
    case '\n':
    case '\t':
    case '\0':
    case ' ':
    case EOF:
        return true;
    default:
        return false;
    }
}

int main(int argc, char **args) {
    init();

    /* declare vars */
    /*   from arguments   */
    int i;
    bool show_source = 0;
    bool show_clean = 0;
    char *filename = NULL;
    /*   for main logic   */
    FILE *fp;
    bool all_done = false;
    bool is_code_mode;
    int current_char = EOF;
    int real_last_char;
    int last_char;
    byte pending_restore_round;
    bool has_ready_char;
    int ready_char = EOF;
    /*   for lex-ing token   */
    int last_ready_char = EOF;
    int last_state = 0;
    struct path_s *current_path;
    struct path_s *test_path;
    char *buff = checked_malloc(sizeof(char), "empty buff");
    int n_buff = 0;
    int lex_loop_state;
#define LEX_LOOP_NORMAL 1
#define LEX_LOOP_PRE_FORCE_WHITESPACE 2
#define LEX_LOOP_FORCE_WHITESPACE 3
    /*   for printing token   */
    bool should_clean_buff;
    token_type current_token_type;
    bool is_ident;

    /* handle the arguments */
    /*   check for --source or --clean as program arguments   */
    for (i = 1; i < argc; i++) {
        /* catch "--source" argument */
        if (strcmp(args[i], "--source") == 0) {
            show_source = 1;
        } /* catch "--clean" argument */
        else if (strcmp(args[i], "--clean") == 0) {
            show_clean = 1;
        } else {
            filename = args[i];
        }
    }
    /*   validate the arguments   */
    if (filename == NULL) {
        printf("\nError: filename is not given!\n");
        printf("Example : %s code.pl0\n", args[0]);
        exit(EXIT_FAILURE);
    }


    /* setup the file pointer */
    fp = fopen(filename, "r");
    if (fp == NULL) {
        printf("\nError: failed to open file \"%s\"\n", filename);
        exit(EXIT_FAILURE);
    }

    /* main logic */
    printf("\n");
    while (!all_done) {
        rewind(fp);
        pending_restore_round = 0;
        has_ready_char = false;

        if (show_source) {
            show_source = false;
            printf("source code:\n");
            printf("------------\n");
            while ((real_last_char = current_char, current_char = fgetc(fp)) != EOF) {
                printf("%c", current_char);
            }
            if (real_last_char != '\n') {
                printf("\n\\ No newline at end of file\n");
            }
            printf("\n");
        } else {
            if (show_clean) {
                printf("source code without comments:\n");
                printf("-----------------------------\n");
            } else {
                /* show lex token */
                printf("tokens:\n");
                printf("-------\n");
            }
            is_code_mode = true;
            last_char = EOF;
            for (;;) {
                /* scanner filtering comment */
                if (pending_restore_round > 0) {
                    has_ready_char = true;
                    if (pending_restore_round == 2) {
                        ready_char = '/';
                    } else { /* pending_restore_round == 1*/
                        ready_char = current_char;
                    }
                    pending_restore_round--;
                } else {
                    real_last_char = current_char;
                    current_char = fgetc(fp);
                    if (is_code_mode) {
                        /* code mode */
                        if (last_char == '/') {
                            if (current_char == '*') {
                                /* enter comment mode */
                                is_code_mode = false;
                                //has_ready_char = false;
                                if (show_clean) {
                                    printf(" ");
                                }
                            } else {
                                /* restore from pending comment mode (to buffered code mode) */
                                //has_ready_char = false;
                                ready_char = last_char;
                                if (current_char == '/') {
                                    has_ready_char = true;
                                } else {
                                    pending_restore_round = 2;
                                }
                            }
                        } else if (current_char != '/') {
                            has_ready_char = true;
                            ready_char = current_char;
                        } else {
                            has_ready_char = false;
                        }
                        last_char = current_char;
                    } else {
                        /* comment mode */
                        if (last_char == '*' && current_char == '/') {
                            /* enter code mode */
                            is_code_mode = true;
                            if (show_clean) {
                                printf(" ");
                            }
                        } else if (last_char == '*' && current_char != '/') {
                            last_char = EOF;
                        } else if (current_char == '*') {
                            last_char = current_char;
                        }
                        ready_char = current_char == EOF
                                     ? EOF
                                     : (is_visible_char(current_char) ? ' ' : current_char);
                        has_ready_char = true;
                    }
                }

                /* code char consume part */
                if (has_ready_char) {
                    has_ready_char = false;
                    if (ready_char == EOF) {
                        if (real_last_char != '\n' && show_clean) {
                            printf("\n\\ No newline at end of file\n");
                        }
                    } else if (show_clean) {
                        printf("%c", ready_char);
                    } else { /* show lex token */
                        lex_loop_state = LEX_LOOP_NORMAL;
                        for (;;) {
                            if (
                                ready_char == '!'
                                || ready_char == '@'
                                || ready_char == '#'
                                || ready_char == '$'
                                || ready_char == '%'
                                || ready_char == '^'
                                || ready_char == '&'
                                || ready_char == '~'
                                || ready_char == '`'
                                || ready_char == '_'
                            ) {
                                printf("\nError: Invalid Token (%c)\n", ready_char);
                                exit(EXIT_FAILURE);
                            }
                            should_clean_buff = false;
                            if (is_whitespace(ready_char)) {
                                ready_char = ' ';
                            }
#ifdef __debug__
                            printf("\nlex-ing '%c'(%u)\n", ready_char, ready_char);
                            printf("loop status = %d\n", lex_loop_state);
#endif
                            current_path = get_path(last_state, ready_char);
                            if (current_path == NULL || is_whitespace(ready_char)) {
                                /* check if it is ident */
                                if (is_alphabet(ready_char) || is_digit(ready_char)) {
                                    is_ident = true;
                                    for (i = 0; i < n_buff; i++) {
                                        if (!(is_alphabet(buff[i]) || is_digit(buff[i]))) {
                                            is_ident = false;
                                            break;
                                        }
                                    }
                                    if (is_ident) {
#ifdef __debug__
                                        printf("\tjump into ident state\n");
#endif
                                        last_state = STATE_IDENT;
                                        continue;
                                    }
                                }
                                if (current_path == NULL) {
                                    if (lex_loop_state == LEX_LOOP_NORMAL) {
#ifdef __debug__
                                        printf("\tforce clean buff\n");
#endif
                                        lex_loop_state = LEX_LOOP_PRE_FORCE_WHITESPACE;
                                    } else {
                                        current_token_type = identsym;
                                        should_clean_buff = true;
                                    }
                                } else { /* this is whitespace */
#ifdef __debug__
                                    printf("\twhitespace\n");
#endif
                                    if (current_path->token_type == PENDING_TOKEN) {
                                        /* use whitespace to advance to complete the pending token */
                                        test_path = get_path(current_path->dest_state, ' ');
                                        if (test_path->token_type != PENDING_TOKEN) {
                                            current_path = test_path;
                                        }
                                    }
                                    if (current_path->token_type != EMPTY_TOKEN) {
                                        current_token_type = current_path->token_type;
                                        should_clean_buff = true;
                                    }
                                }
                            } else {
                                /* wait and read more */
                                buff = checked_realloc(buff, sizeof(char) * (++n_buff + 1), "buff for token");
                                buff[n_buff - 1] = (char) ready_char;
                                buff[n_buff] = '\0';
                                last_state = current_path->dest_state;
                            }
                            if (should_clean_buff) {
                                /* probably an identifier, start with token-like prefix */
                                /* check if it is an identifier */
                                if (current_token_type == numbersym) {
                                    i = number_value(buff, n_buff);
                                    if (i == -1) {
                                        printf("\nError: invalid token, number expected, but get '%s'\n", buff);
                                        exit(EXIT_FAILURE);
                                    } else if (i == -2) {
                                        printf("\nError: invalid token, number(%s) MAX_NUMBER_VALUE(%d)\n",
                                               buff, MAX_NUMBER_VALUE);
                                        exit(EXIT_FAILURE);
                                    }
                                } else if (current_token_type == identsym) {
                                    if (n_buff > MAX_IDENTIFIER_LENGTH) {
                                        printf("\nError: Identifier '%s' is longer than 12 char (%d)\n", buff, n_buff);
                                        exit(EXIT_FAILURE);
                                    }
                                    if (!is_alphabet(buff[0])) {
                                        printf("\nError: Invalid Identifier '%s', identifier cannot start with number\n",
                                               buff);
                                        exit(EXIT_FAILURE);
                                    }
                                }
                                /* print token */
                                for (i = 0; i < 12; i++) {
                                    if (i < n_buff) {
                                        printf("%c", buff[i]);
                                    } else {
                                        printf(" ");
                                    }
                                }
                                printf(" %d\n", current_token_type);
                                last_state = 0;
                                n_buff = 0;
                                buff[0] = '\0';
                            }
#ifdef __debug__
                            printf("state = %d, buff = '%s'\n", last_state, buff);
#endif
                            /* ending of lex loop */
                            if (lex_loop_state == LEX_LOOP_PRE_FORCE_WHITESPACE) {
                                last_ready_char = ready_char;
                                ready_char = ' ';
                                lex_loop_state = LEX_LOOP_FORCE_WHITESPACE;
                            } else if (lex_loop_state == LEX_LOOP_FORCE_WHITESPACE) {
                                ready_char = last_ready_char;
                                lex_loop_state = LEX_LOOP_NORMAL;
                            } else { /* lex_loop_state == LEX_LOOP_NORMAL */
                                break;
                            }
                        }
                    }
                }
                if (current_char == EOF && pending_restore_round <= 0)
                    break;
            }
            if (is_code_mode == 0) {
                if (!show_clean) {
                    printf("\nError: unexpected token 'EOF', the last comment is not closed\n");
                    exit(EXIT_FAILURE);
                }
            }
            if (show_clean) {
                show_clean = false;
                printf("\n");
            } else { /* show lex token */
                all_done = true;
            }
        }
    }

    fclose(fp);

    return EXIT_SUCCESS;
}
