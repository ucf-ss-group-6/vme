/* This file was automatically generated.  Do not edit! */
void debug_leave(string name);
void debug_enter(string name);
void debug_print_indent();
extern int debug_n_indent;
void check_and_fix_newline(char *filename);
char get_last_char_in_file(char *filename);
string replace_const_string(string dest,const string src);
string replace_string(string dest,string src);
bool is_str_same(const string a,const string b);
string clone_const_string(const string input);
string clone_string(string input);
struct str_to_long_s *str_to_long(char *str);
bool is_whitespace(int c);
bool is_symbol(int c);
bool is_alphabet(int c);
int number_value(char *cs,int n);
bool is_digit(int c);
bool is_visible_char(int c);
extern const int MAX_NUMBER_VALUE;
extern const int MAX_IDENTIFIER_LENGTH;
FILE *checked_fopen(char *filename,char *mode);
void *checked_realloc(void *dest,size_t size,const char *name);
void *checked_malloc(size_t size,const char *name);
void dummy_printf(char *pattern,...);
