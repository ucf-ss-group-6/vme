/*
 * bundled compiler, do lexer, parsing and code_gen at once
 * */

#include <stdlib.h>
#include <stdio.h>
#include "utils.c"
#include "error.h"
#include "compile.h"
#include "pm0code.h"
#include "symbol_table.c"

#undef exit
#define exit deinit();fflush(stdout);exit

/* array def */
struct code_s {
    int op;
    int l;
    int m;
};

/* array, pending init in init() */
struct code_s *codes;
/* array size */
int n_code = 0;

int line = 1;
int col = 0;
int last_col = 0;
int tok; // not used

int err_code = 0;
/* error type def */
const byte PARSER_ERROR = 1;
const byte LEXER_ERROR = 2;
/* error type var */
byte err_type = 0;

FILE *fp_source_code;

/* stack def */
struct err_msg_s {
    string msg;
    struct err_msg_s *next;
    int line;
    int col;
};
/* stack top (opposite of root) var */
struct err_msg_s *err_msg_p = NULL;

void push_err_msg(string msg) {
    debug_print_indent();
    debug_printf("enter:push_err_msg(%s)\n", msg);
    debug_n_indent++;
    struct err_msg_s *p = checked_malloc(sizeof(struct err_msg_s), "struct err_msg_s in push_err_msg()");
    p->msg = msg;
    p->next = err_msg_p;
    p->line = line;
    p->col = col;
    err_msg_p = p;
    debug_leave("push_err_msg()");
}

void pop_err_msg() {
    debug_enter("pop_err_msg()");
    if (err_msg_p == NULL) {
        debug_printf("Attempt to pop err msg on empty stack!\n");
    } else {
        free(err_msg_p->msg);
        err_msg_p = err_msg_p->next;
    }
    debug_leave("pop_err_msg()");
}

void error() {
    string error_type_name = (err_type == LEXER_ERROR)
                             ? "Lexer"
                             : (err_type == PARSER_ERROR)
                             ? "Parser"
                             : "Undefined";
    const string err_msg = (err_type == LEXER_ERROR)
                           ? LEXER_ERROR_MSGs[err_code]
                           : (err_type == PARSER_ERROR)
                           ? PARSER_ERROR_MSGs[err_code]
                           : "";
    fflush(stdout);
    fprintf(stderr, "Error(%d:%d): %s Error Code %d : %s\n", line, col, error_type_name, err_code, err_msg);
    struct err_msg_s *p;
    int i = 0;
    int i2;
    for (p = err_msg_p; p != NULL; p = p->next, i++) {
        /* print indentation */
        for (i2 = 0; i2 <= i; i2++) {
            fprintf(stderr, "  ");
        }
        /* print stacked message */
        fprintf(stderr, "(%d:%d): %s\n", p->line, p->col, p->msg);
        free(p->msg);
    }
    if (err_code == 0) {
        exit(1);
    } else {
        exit(err_code);
    }
}

void push_lexer_error(int idx) {
    err_type = LEXER_ERROR;
    err_code = idx;
    push_err_msg(clone_const_string(LEXER_ERROR_MSGs[idx]));
}

void push_parser_error(int idx) {
    err_type = PARSER_ERROR;
    err_code = idx;
    push_err_msg(clone_const_string(PARSER_ERROR_MSGs[idx]));
}

/* ---- begin lex & parse ---- */

/**
 * will not skip comment
 * */
char getChar_raw() {
    last_col = col;
    char c = fgetc(fp_source_code);
    if (c == '\n') {
        line++;
        col = 1;
    } else {
        col++;
    }
    return c;
}

/**
 * will skip comment
 * */
char getChar() {
    char c1 = getChar_raw();
    if (c1 != '/') {
        return c1;
    }
    char c2 = getChar_raw();
    if (c2 != '*') {
        unGetChar(c2);
        return c1;
    }
    char c3;
L1:
    for (;;) {
        c3 = getChar_raw();
        if (c3 == '*') {
            break;
        }
        if (c3 == EOF) {
            fflush(stdout);
            fflush(stderr);
            fprintf(stderr, "Error: comment not closed!\n");
            exit(1);
        }
    }
    char c4 = getChar_raw();
    if (c4 == '/') {
        unGetChar(' ');
        return getChar_raw();
    } else {
        goto L1;
    }
}

/**
 * @remark cannot fall back more than one line
 * */
void unGetChar(char c) {
    //    debug_printf("unGetChar(%c)\n", c);
    ungetc(c, fp_source_code);
    if (c == '\n') {
        line--;
        col = last_col;
    } else {
        col--;
    }
}

/**
 * @remark resulting buffer's whitespace might not align with source code (will mess up line and col)
 * */
void unGetWord(string s) {
    int n = strlen(s);
    int i;
    for (i = n - 1; i >= 0; i--) {
        unGetChar(s[i]);
    }
}

void unGetNumber(int value) {
    bool is_negative = false;
    if (value == 0) {
        unGetChar('0');
        return;
    }
    if (value < 0) {
        is_negative = true;
        value = -value;
    }
    for (;;) {
        if (value == 0) {
            break;
        }
        unGetChar('0' + (value % 10));
        value /= 10;
    }
    if (is_negative) {
        unGetChar('-');
    }
}

bool parse_char(char c, char *result) {
    *result = getChar();
    return c == *result;
}

void advance() {
}

/**
 * @remark memory leak
 * @remark will push error
 * */
bool eat_char(char c) {
    char res = getChar();
    if (res != c) {
        unGetChar(res);
        string temp = "Expected char '%c' but seen '%c'.";
        string msg = checked_malloc(sizeof(char) * (strlen(temp) - 2 * 2 + 2 + 1), "err_msg in eat_char()");
        sprintf(msg, temp, c, res);
        push_err_msg(msg);
        return false;
    }
    return true;
}

/**
 * quiet version of eat_char()
 * */
bool try_eat_char(char c) {
    char res = getChar();
    if (res != c) {
        unGetChar(res);
        return false;
    }
    return true;
}

/**
 * @remark memory leak when the error message is pop out later on
 * @remark will push error
 * */
bool parse_word(string word) {
    debug_print_indent();
    debug_printf("enter:parse_word(%s)\n", word);
    debug_n_indent++;
    int n = strlen(word);
    int i;
    bool res = true;
    string result = checked_malloc(sizeof(char) * (n + 1), "result in parse_word()");
    skip_whitespace();
    for (i = 0; i < n; i++) {
        if (!parse_char(word[i], result + i)) {
            /* failed, back track all the way */
            int i2;
            for (i2 = i; i2 >= 0; i2--) {
                unGetChar(result[i2]);
            }
            string temp = "Expect \"%s\" but seen \"%s\"";
            int n = strlen(temp) - 2 * 2 + strlen(word) + (i + 1) + 1;
            string msg = checked_malloc(sizeof(char) * n, "error message in parse_word()");
            result[i + 1] = '\0';
            sprintf(msg, temp, word, result);
            push_err_msg(msg);
            res = false;
            break;
        }
    }
    free(result);
    debug_leave("parse_word()");
    return res;
}

/**
 * quiet version of parse_word()
 * */
bool try_parse_word(string word) {
    debug_print_indent();
    debug_printf("enter:try_parse_word(%s)\n", word);
    debug_n_indent++;
    int n = strlen(word);
    int i;
    bool res = true;
    string result = checked_malloc(sizeof(char) * strlen(word), "result in try_parse_word()");
    skip_whitespace();
    for (i = 0; i < n; i++) {
        if (!parse_char(word[i], result + i)) {
            /* failed, back track all the way */
            int i2;
            for (i2 = i; i2 >= 0; i2--) {
                unGetChar(result[i2]);
            }
            res = false;
            break;
        }
    }
    free(result);
    debug_n_indent--;
    debug_print_indent();
    debug_printf("leave:try_parse_word():%s\n", res ? "true" : "false");
    return res;
}

/**
 * @remark won't stop if the char is not exist
 * */
void skip_char_until(char c) {
    char res;
    for (res = getChar(); res != c; res = getChar());
    unGetChar(res);
}

/**
 * @remark won't stop if the char is not exist
 * */
void skip_char_include(char c) {
    char res;
    for (res = getChar(); res != c; res = getChar());
}

/**
 * skip whitespace, expect EOF
 * TODO skip comment as well
 * @return bool : always return true for chained op
 * TODO return consumed string for rollback
 * */
bool skip_whitespace() {
    debug_enter("skip_whitespace()");
    char res;
    for (;;) {
        res = getChar();
        if (res == EOF) {
            unGetChar(EOF);
            break;
        }
        if (!is_whitespace(res)) {
            unGetChar(res);
            break;
        }
    }
    debug_leave("skip_whitespace()");
    return true;
}

bool eat_whitespace() {
    char c = getChar();
    bool res;
    if (!(res = is_whitespace(c))) {
        unGetChar(c);
    }
    return res;
}

/**
 * grow codes array and store new code
 * @return index of the emitted code
 * */
int emit(int op, int l, int m) {
    n_code++;
    codes = checked_realloc(codes, sizeof(struct code_s) * n_code, "codes in emit()");
    codes[n_code - 1].op = op;
    codes[n_code - 1].l = l;
    codes[n_code - 1].m = m;
    return n_code - 1;
}

string new_ident_string() {
    string name = checked_malloc(sizeof(char) * (MAX_NUMBER_VALUE + 1), "new_ident_string()");
    int i;
    for (i = 0; i < MAX_IDENTIFIER_LENGTH + 1; i++) {
        name[i] = 0;
    }
    return name;
}

bool parse_program() {
    debug_enter("parse_program()");
    bool res = parse_block()
               && parse_period();
    leave_level();
    if (!res) {
        push_err_msg(clone_const_string("Failed to parse program."));
    }
    debug_leave("parse_program()");
    return res;
}

//const int BLOCK_MAIN = 1;
//const int BLOCK_PROCEDURE = 2;
//const int BLOCK_DEFAULT = 3;

/**
 * @param offset : start of body (after procedure)
 * */
bool parse_block() {
    debug_enter("parse_block()");
    create_new_level();
    int pending_JMP = emit(JMP, 0, 0);
    bool res = parse_const_declaration()
               && parse_var_declaration()
               && parse_procedure_declaration()
               && ((codes[pending_JMP].m = n_code) || Bool(true))
               && ((emit(INC, 0, lex_level_head->n_var + 4)) || Bool(true))
               && parse_statement();
    if (!res) {
        push_err_msg(clone_const_string("Failed to parse block."));
    }
    leave_level();
    debug_leave("parse_block()");
    return res;
}

bool parse_const_declaration() {
    debug_enter("parse_const_declaration()");
    string name = new_ident_string();
    int value;
    bool defer(bool res) {
        free(name);
        debug_leave("parse_const_declaration()");
        return res;
    }
    if (!try_parse_word("const")) {
        return defer(true);
    }
    for (;;) {
        /* at least once */
        if (!parse_ident(name)) {
            push_parser_error(4);
            return defer(false);
        }
        if (!parse_eq()) {
            push_parser_error(3);
            return defer(false);
        }
        if (!parse_number(&value)) {
            push_parser_error(2);
            return defer(false);
        }
        store_constant(name, value);
        skip_whitespace();
        if (!try_eat_char(',')) {
            break;
        }
    }
    skip_whitespace();
    return defer(parse_semicolon());
}

/**
 * @deprecated negative example
 * */
bool parse_const_declaration_old() {
    debug_enter("parse_const_declaration()");
    bool res = true;
    string name = new_ident_string();
    int value;
    skip_whitespace();
    if (try_parse_word("const")) {
        debug_printf("Found const block\n");
        if (!(res = parse_ident(name))) {
            push_parser_error(4);
        } else {
            skip_whitespace();
            if (!(res = eat_char('='))) {
                push_parser_error(3);
            } else {
                if (!(res = parse_number(&value))) {
                    push_parser_error(2);
                } else {
                    for (;;) {
                        if (try_eat_char(',')) {
                            if (!(res = parse_ident(name))) {

                            } else {

                            }
                        } else {
                            break;
                        }
                    }
                }
            }
        }
    } else {
        debug_printf("Not found const block\n");
    }
    free(name);
    debug_leave("parse_const_declaration()");
    return res;
}

/**
 * @remark instead of using defer, using goto label
 * */
bool parse_var_declaration() {
    debug_enter("parse_var_declaration()");
    bool res = true;
    string name = new_ident_string();
    skip_whitespace();
    if (try_parse_word("var")) {
        /* optional "var" is seen */
        debug_printf("Found var block\n");
        skip_whitespace();
        if (!parse_ident(name)) {
            res = false;
            goto done;
        }
        register_var(name);
        for (;;) {
            skip_whitespace();
            if (!try_eat_char(',')) {
                /* optional ',' not seen */
                break;
            }
            skip_whitespace();
            if (!parse_ident(name)) {
                push_parser_error(31);
                res = false;
                break;
            }
            register_var(name);
        }
        skip_whitespace();
        res &= parse_semicolon();
    } else {
        debug_printf("Not found var block\n");
    }
done:
    free(name);
    debug_leave("parse_var_declaration()");
    return res;
}

/**
 * used fallback to get effect of function composite
 * */
bool parse_procedure_declaration() {
    debug_enter("parse_procedure_declaration()");
    string name = new_ident_string();
    bool defer(bool res) {
        free(name);
        debug_leave("parse_procedure_declaration()");
        return res;
    }
    for (;;) {
        skip_whitespace();
        if (!try_parse_word("procedure")) {
            return defer(true);
        }
        debug_printf("Found procedure block\n");
        bool fallback1(bool res) {
            unGetWord(" procedure ");
            return defer(res);
        }
        skip_whitespace();
        if (!parse_ident(name)) {
            push_parser_error(4);
            return fallback1(false);
        }
        bool fallback2(bool res) {
            unGetChar(' ');
            unGetWord(name);
            unGetChar(' ');
            return fallback1(res);
        }
        skip_whitespace();
        if (!parse_semicolon()) {
            return fallback2(false);
        }
        bool fallback3(bool res) {
            unGetChar(';');
            return fallback2(res);
        }
        skip_whitespace();
        register_procedure(name, n_code); // offset by one for the INC statement
        if (!parse_block()) {
            return fallback3(false);
        }
        // TODO define fallback to undo the parse block
        if (!parse_semicolon()) {
            return fallback3(false);
        }
        emit(OPR, 0, RET);
    }
}

bool parse_statement() {
    debug_enter("parse_statement()");
    string name = new_ident_string();
    bool defer(bool res) {
        if (!res) {
            push_err_msg(clone_const_string("Failed to parse statement."));
        }
        free(name);
        debug_leave("parse_statement()");
        return res;
    }
    int level = 0;
    int offset = 0;
    int addr = 0;
    int level_and_offset[] = {0, 0};
    bool too_long;
    skip_whitespace();
    debug_print_indent();
    if (try_parse_ident(name, &too_long)) {
        debug_printf("parsing assignment\n");
        skip_whitespace();
        if (!parse_assign()) {
            push_parser_error(13);
            return defer(false);
        }
        if (!get_var_offset(name, level_and_offset)) {
            /* var not registered */
            /* check if the ident is procedure or const */
            if (get_constant(name, &offset) || get_procedure_address(name, level_and_offset)) {
                push_parser_error(12);
                return defer(false);
            }
            string temp = "Cannot not find var \"%s\".";
            int n = strlen(temp) - 2 + strlen(name) + 1;
            string msg = checked_malloc(sizeof(char) * n, "error message");
            sprintf(msg, temp, name);
            push_err_msg(clone_string(msg));
            free(msg);
            push_parser_error(37);
            return defer(false);
        }
        level = level_and_offset[0];
        offset = level_and_offset[1];
        /* success to get var info */
        skip_whitespace();
        if (!parse_expression()) {
            return defer(false);
        }
        // assignment (save value to ident)
        emit(STO, level, offset);
    } else if (too_long) {
        push_lexer_error(4);
        return defer(false);
    } else if (try_parse_word("call")) {
        debug_print_indent("parsing call subroutine\n");
        skip_whitespace();
        if (!parse_ident(name)) {
            push_parser_error(14);
            return defer(false);
        }
        if (!get_procedure_address(name, level_and_offset)) {
            /* failed */
            if (get_var_offset(name, level_and_offset) || get_constant(name, &addr)) {
                /* that is a const or var */
                push_parser_error(15);
                return defer(false);
            }
            /* that is not const nor var */
            push_parser_error(38);
            return defer(false);
        }
        /* success to get procedure */
        emit(CAL, level_and_offset[0], level_and_offset[1]);
    } else if (try_parse_word("begin")) {
        debug_printf("checking begin-end block\n");
        skip_whitespace();
        if (!parse_statement()) {
            push_parser_error(7);
            return defer(false);
        }
        for (;;) {
            skip_whitespace();
            if (!try_eat_char(';')) {
                break;
            }
            skip_whitespace();
            if (!parse_statement()) {
                return defer(false);
            }
        }
        skip_whitespace();
        if (!(parse_word("end"))) {
            push_parser_error(34);
            return defer(false);
        }
    } else if (try_parse_word("if")) {
        debug_printf("checking if block\n");

        skip_whitespace();
        if (!parse_condition()) {
            push_parser_error(36);
            return defer(false);
        }

        skip_whitespace();
        if (!parse_word("then")) {
            return defer(false);
        }

        int pending_JPC = emit(JPC, 0, 0);

        skip_whitespace();
        if (!parse_statement()) {
            return defer(false);
        }

        skip_whitespace();
        bool has_else = try_parse_word("else");

        if (has_else) {
            int pending_JMP = emit(JMP, 0, 0);
            codes[pending_JPC].m = n_code;
            skip_whitespace();
            if (!parse_statement()) {
                return defer(false);
            }
            codes[pending_JMP].m = n_code;
        } else {
            /* no else */
            codes[pending_JPC].m = n_code;
        }
    } else if (try_parse_word("while")) {
        debug_printf("checking while block\n");

        int pre_condition = n_code;

        skip_whitespace();
        if (!parse_condition()) {
            push_parser_error(36);
            return defer(false);
        }

        skip_whitespace();
        if (!parse_word("do")) {
            push_parser_error(18);
            return defer(false);
        }

        int pending_JPC = emit(JPC, 0, 0);

        skip_whitespace();
        if (!parse_statement()) {
            push_parser_error(7);
            return defer(false);
        }

        emit(JMP, 0, pre_condition);
        codes[pending_JPC].m = n_code;
    } else if (try_parse_word("read")) {
        debug_printf("checking read statement\n");

        emit(SIO, 0, INP);

        skip_whitespace();
        if (!parse_ident(name)) {
            return defer(false);
        }

        if (!get_var_offset_with_error(name, level_and_offset)) {
            if (err_type == PARSER_ERROR && err_code == 39) {
                push_parser_error(12);
            }
            return defer(false);
        }
        level = level_and_offset[0];
        offset = level_and_offset[1];
        emit(STO, level, offset);
    } else if (try_parse_word("write")) {
        debug_printf("checking write statement\n");

        skip_whitespace();
        if (!parse_ident(name)) {
            return defer(false);
        }

        int idx;
        if (!load_var_or_const(name, &idx)) {
            if (try_parse_number(&idx, &too_long)) {
                push_parser_error(41);
            } else {
                push_parser_error(40);
            }
            return defer(false);
        }

        emit(SIO, 0, OUT);
    } else {
        debug_printf("empty statement\n");
    }
    return defer(true);
}

bool get_var_offset_with_error(string name, int *level_and_offset) {
    if (!get_var_offset(name, level_and_offset)) {
        /* failed to get var */
        bool too_long;
        if (try_parse_number(level_and_offset, &too_long)) {
            unGetNumber(level_and_offset[0]);
            string temp = "Seen a number %d.";
            int n = strlen(temp) - 2 + 12 + 1;
            string msg = checked_malloc(sizeof(char) * n, "error message");
            sprintf(msg, temp, level_and_offset[0]);
            push_err_msg(clone_string(msg));
            free(msg);
            push_parser_error(39);
        } else {
            string temp = "Cannot not find var \"%s\".";
            int n = strlen(temp) - 2 + strlen(name) + 1;
            string msg = checked_malloc(sizeof(char) * n, "error message");
            sprintf(msg, temp, name);
            push_err_msg(clone_string(msg));
            free(msg);
            push_parser_error(37);
        }
        return false;
    }
    return true;
}

bool load_var(string name, int *emitted_code_idx) {
    int level_and_offset[] = {0, 0};
    if (get_var_offset(name, level_and_offset)) {
        *emitted_code_idx = emit(LOD, level_and_offset[0], level_and_offset[1]);
        return true;
    }
    return false;
}

bool load_const(string name, int *emitted_code_idx) {
    int value;
    if (get_constant(name, &value)) {
        *emitted_code_idx = emit(LIT, 0, value);
        return true;
    }
    return false;
}

/**
 * will NOT push error
 * */
bool load_var_or_const(string name, int *emitted_code_idx) {
    debug_enter("load_var_or_const()");
    bool res = load_var(name, emitted_code_idx)
               || load_const(name, emitted_code_idx);
    debug_print_indent();
    debug_printf("res = %s\n", res ? "true" : "false");
    debug_leave("load_var_or_const()");
    return res;
}

// TODO
bool parse_condition() {
    debug_enter("parse_condition()");
    string name_l = new_ident_string();
    string name_r = new_ident_string();
    bool defer(bool res) {
        free(name_l);
        free(name_r);
        debug_leave("parse_condition()");
        return res;
    }
    skip_whitespace();
    if (try_parse_word("odd")) {
        skip_whitespace();
        if (!parse_expression()) {
            return defer(false);
        }
        emit(OPR, 0, ODD);
        return defer(true);
    }
    int op_type = 0;
    bool res = parse_expression()
               && parse_rel_op(&op_type)
               && parse_expression();
    emit(OPR, 0, op_type);
    return defer(res);
}

/**
 * @remark will NOT emit code
 * */
bool parse_rel_op(int *op_type) {
    if (try_eat_char('=')) {
        *op_type = EQL;
        return true;
    }
    if (try_eat_char('<')) {
        if (try_eat_char('>')) {
            *op_type = NEQ;
            return true;
        }
        if (try_eat_char('=')) {
            *op_type = LEQ;
            return true;
        }
        *op_type = LSS;
        return true;
    }
    if (try_eat_char('>')) {
        if (try_eat_char('=')) {
            *op_type = GEQ;
            return true;
        }
        *op_type = GTR;
        return true;
    }
    return false;
}

bool parse_rel_op_old() {
    skip_whitespace();
    if (parse_eq()) {
    } else if (parse_neq()) {

    } else if (parse_lt()) {

    } else if (parse_leq()) {

    } else if (parse_gt()) {

    } else if (parse_geq()) {

    } else {
        return false;
    }
    return true;
}

bool parse_expression() {
    debug_enter("parse_expression()");
    bool defer(bool res) {
        debug_leave("parse_expression()");
        return res;
    }
    skip_whitespace();
    bool is_negative = false;
    if (!try_eat_char('+')) {
        is_negative = try_eat_char('-');
    }
    if (!parse_term()) {
        push_parser_error(24);
        return defer(false);
    }
    if (is_negative) {
        emit(OPR, 0, NEG);
    }
    char sign;
    for (;;) {
        skip_whitespace();
        sign = getChar();
        if (sign != '+' && sign != '-') {
            unGetChar(sign);
            break;
        }
        skip_whitespace();
        if (!parse_term()) {
            return defer(false);
        }
        emit(OPR, 0, sign == '+' ? ADD : SUB);
    }
    return defer(true);
}

bool parse_term() {
    debug_enter("parse_term()");
    bool defer(bool res) {
        debug_leave("parse_term()");
        return res;
    }
    if (!parse_factor()) {
        return defer(false);
    }
    char op;
    for (;;) {
        skip_whitespace();
        op = getChar();
        if (op != '*' && op != '/') {
            unGetChar(op);
            return defer(true);
        }
        if (!parse_factor()) {
            return defer(false);
        }
        emit(OPR, 0, op == '*' ? MUL : DIV);
    }
}

/**
 * @remark will push error
 * */
bool parse_factor() {
    debug_enter("parse_factor()");
    string name = new_ident_string();
    bool defer(bool res) {
        free(name);
        debug_leave("parse_factor()");
        return res;
    }
    bool invalid;
    int value;
    int code_idx;
    skip_whitespace();
    if (try_parse_ident(name, &invalid)) {
        if (!load_var_or_const(name, &code_idx)) {
            push_parser_error(40);
            return defer(false);
        }
    } else if (invalid) {
        push_lexer_error(4);
        return defer(false);
    } else if (try_parse_number(&value, &invalid)) {
        emit(LIT, 0, value);
    } else if (invalid) {
        push_lexer_error(3);
        return defer(false);
    } else if (try_eat_char('(')) {
        skip_whitespace();
        if (!parse_expression()) {
            return defer(false);
        }
        skip_whitespace();
        return defer(eat_char(')'));
    } else {
        push_parser_error(23);
        push_err_msg(clone_const_string("Failed to parse factor"));
        return defer(false);
    }
    return defer(true);
}

/**
 * @remark will push error
 * */
bool parse_number(int *acc) {
    bool res = true;
    skip_whitespace();
    char c;
    if (!(res = parse_digit(&c))) {
        /* not a number */
        push_lexer_error(2);
    } else {
        *acc = c - '0';
        for (;;) {
            if (parse_digit(&c)) {
                *acc = (*acc) * 10 + (c - '0');
            } else {
                break;
            }
        }
        if (*acc > MAX_NUMBER_VALUE) {
            push_lexer_error(3);
            return false;
        }
        debug_printf("seen number:%d\n", *acc);
    }
    return res;
}

/**
 * quiet version of parse_number()
 * @remark will NOT push error
 * */
bool try_parse_number(int *acc, bool *too_large) {
    *too_large = false;
    bool res = true;
    skip_whitespace();
    char c;
    if (!(res = parse_digit(&c))) {
        /* not a number */
        return false;
    } else {
        *acc = c - '0';
        for (;;) {
            if (parse_digit(&c)) {
                *acc = (*acc) * 10 + (c - '0');
            } else {
                break;
            }
        }
        if (*acc > MAX_NUMBER_VALUE) {
            *too_large = true;
            return false;
        }
        debug_printf("seen number:%d\n", *acc);
    }
    return res;
}

bool is_preserved_word(string s) {
    bool res = is_str_same(s, "const")
               || is_str_same(s, "var")
               || is_str_same(s, "procedure")
               || is_str_same(s, "call")
               || is_str_same(s, "begin")
               || is_str_same(s, "end")
               || is_str_same(s, "if")
               || is_str_same(s, "then")
               || is_str_same(s, "else")
               || is_str_same(s, "while")
               || is_str_same(s, "do")
               || is_str_same(s, "read")
               || is_str_same(s, "write")
               || is_str_same(s, "odd");
    debug_print_indent();
    debug_printf("is_preserved_word(%s):%s\n", s, res ? "true" : "false");
    return res;
}


/**
 * @remark will push error
 * TODO this function should not skip whitespace, it should be done by caller for rollback
 * */
bool parse_ident(string name) {
    debug_enter("parse_ident()");
    int len = 0;
    skip_whitespace();
    bool res;
    if ((res = parse_letter(name))) {
        len++;
        for (;;) {
            if (eat_whitespace()) {
                break;
            }
            if (parse_letter(name + len) || parse_digit(name + len)) {
                len++;
                if (len > MAX_IDENTIFIER_LENGTH) {
                    res = false;
                    push_lexer_error(4);
                    break;
                }
            } else {
                break;
            }
        }
        if (res) {
            name[len] = '\0';
            if (is_preserved_word(name)) {
                push_parser_error(31);
                res = false;
            }
        }
    } else {
        char result;
        if (parse_digit(&result)) {
            push_lexer_error(5);
        } else {
            push_parser_error(31);
        }
        unGetChar(result);
    }
    if (!res) {
        int i;
        for (i = len - 1; i >= 0; i--) {
            unGetChar(name[i]);
        }
    } else {
        debug_printf("seen ident:%s\n", name);
    }
    debug_leave("parse_ident()");
    return res;
}

/**
 * @remark quiet version of parse_ident()
 * TODO this function should not skip whitespace, it should be done by caller for rollback
 * */
bool try_parse_ident(string name, bool *too_long) {
    debug_enter("try_parse_ident()");
    *too_long = false;
    int len = 0;
    skip_whitespace();
    bool res;
    if ((res = parse_letter(name + 0))) {
        len++;
        for (;;) {
            if (eat_whitespace()) {
                break;
            }
            if (parse_letter(name + len) || parse_digit(name + len)) {
                len++;
                if (len > MAX_IDENTIFIER_LENGTH) {
                    res = false;
                    *too_long = true;
                    break;
                }
            } else {
                break;
            }
        }
        if (res) {
            name[len] = '\0';
            if (is_preserved_word(name)) {
                res = false;
            }
        }
    }
    if (!res) {
        int i;
        for (i = len - 1; i >= 0; i--) {
            debug_printf("len = %d, i = %d, c = %c\n", len, i, name[i]);
            unGetChar(name[i]);
        }
    } else {
        debug_printf("seen ident:%s\n", name);
    }
    debug_leave("try_parse_ident()");
    return res;
}

/**
 * @remark will NOT push error
 * */
bool parse_digit(char *result) {
    debug_enter("parse_digit()");
    *result = getChar();
    if ('0' <= *result && *result <= '9') {
        debug_leave("parse_digit()");
        return true;
    } else {
        unGetChar(*result);
        debug_leave("parse_digit()");
        return false;
    }
}

/**
 * @remark will NOT push error
 * */
bool parse_letter(char *result) {
    debug_enter("parse_letter()");
    *result = getChar();
    if (
        ('a' <= *result && *result <= 'z')
        ||
        ('A' <= *result && *result <= 'Z')
    ) {
        debug_leave("parse_letter()");
        return true;
    } else {
        unGetChar(*result);
        debug_leave("parse_letter()");
        return false;
    }
}

/**
 * @remark will push error
 * */
bool parse_semicolon() {
    skip_whitespace();
    char c = getChar();
    if (c == ';') {
        return true;
    } else {
        unGetChar(c);
        push_parser_error(32);
        return false;
    }
}

/**
 * @remark will push error
 * */
bool parse_comma() {
    skip_whitespace();
    char c = getChar();
    if (c == ',') {
        return true;
    } else {
        unGetChar(c);
        push_parser_error(33);
        return false;
    }
}

/**
 * @remark will push error
 * */
bool parse_period() {
    skip_whitespace();
    bool res = eat_char('.');
    if (!res) {
        push_parser_error(9);
    }
    return res;
}

/**
 * @remark will push error
 * */
bool parse_eq() {
    skip_whitespace();
    return eat_char('=');
}

bool parse_neq() {
    skip_whitespace();
    return parse_word("<>");
}

bool parse_lt() {
    skip_whitespace();
    bool res;
    if (try_parse_word("<=")) {
        unGetWord("<=");
        res = false;
    } else {
        res = eat_char('<');
    }
    return res;
}

bool parse_leq() {
    skip_whitespace();
    return parse_word("<=");
}

bool parse_gt() {
    skip_whitespace();
    bool res;
    if (try_parse_word(">=")) {
        unGetWord(">=");
        res = false;
    } else {
        res = eat_char('>');
    }
    return res;
}

bool parse_geq() {
    skip_whitespace();
    return parse_word(">=");
}

bool parse_assign() {
    skip_whitespace();
    bool res;
    if (!(res = parse_word(":="))) {
        if (try_eat_char('=')) {
            unGetChar('=');
            pop_err_msg();
            push_lexer_error(1);
        }
    }
    return res;
}

/* ---- end lex & parse ---- */


/* ---- begin ast & code generation ---- */

//TODO

/* ---- end ast & code generation ---- */

void init() {
    debug_enter("init()");
    codes = malloc(0);
    init_symbol_table();
    debug_leave("init()");
}

void deinit() {
    static bool first = true;
    if (first) {
        first = false;
        fclose(fp_source_code);
    }
}

int main(int argc, char **args) {
    if (argc != 1 + 2) {
        fflush(stdout);
        fprintf(stderr, "\nError: invalid arguments\n");
        fprintf(stderr, "example: %s code.pl0 code.pm0\n", args[0]);
        exit(EXIT_FAILURE);
    }
    string plcode_filename = args[1];
    string pmcode_filename = args[2];
    FILE *fp_pm0;
    int i;

    init();

    fp_source_code = fopen(plcode_filename, "r");
    if (fp_source_code == NULL) {
        fflush(stdout);
        fprintf(stderr, "\nError: failed to open source code file: %s\n", plcode_filename);
        exit(1);
    }


    /* ---- main body ---- */
    /* parse source code (and generate code) */
    debug_printf("start to parse source code\n");
    if (!parse_program()) {
        fprintf(stderr, "Failed to compile %s\n", plcode_filename);
        error(); // will exit directly
    }
    emit(SIO, 0, HLT);

    debug_printf("parsed without error.\n");

    /* save generated code */
    debug_printf("start to save generated code\n");
    fp_pm0 = fopen(pmcode_filename, "w");
    if (fp_pm0 == NULL) {
        fflush(stdout);
        fprintf(stderr, "\nError: failed to open pm0 file to save the code: %s\n", pmcode_filename);
        exit(1);
    }
    for (i = 0; i < n_code; i++) {
        fprintf(fp_pm0, "%d %d %d\n", codes[i].op, codes[i].l, codes[i].m);
    }
    debug_printf("finished saving generated code\n");

    fclose(fp_pm0);
    deinit();
    printf("Compiled \"%s\" without error, saved to \"%s\".\n", plcode_filename, pmcode_filename);
    fflush(stdout);
    fflush(stderr);
    return EXIT_SUCCESS;
}