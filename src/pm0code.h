/* begin: machine code alias */
const int LIT = 1;
const int OPR = 2;
const int LOD = 3;
const int STO = 4;
const int CAL = 5;
const int INC = 6;
const int JMP = 7;
const int JPC = 8;
const int SIO = 9;
/* op code 2 */
const int RET = 0;
const int NEG = 1;
const int ADD = 2;
const int SUB = 3;
const int MUL = 4;
const int DIV = 5;
const int ODD = 6;
const int MOD = 7;
const int EQL = 8;
const int NEQ = 9;
const int LSS = 10;
const int LEQ = 11;
const int GTR = 12;
const int GEQ = 13;
/* op code 9 */
const int OUT = 0;
const int INP = 1;
const int HLT = 2;
/* end: machine code alias */

const char *OPCODE_NAMES[] = {
    "Undefined OPCODE_NAME",
    /* 1 */ "LIT",
    /* 2 */ "OPR",
    /* 3 */ "LOD",
    /* 4 */ "STO",
    /* 5 */ "CAL",
    /* 6 */ "INC",
    /* 7 */ "JMP",
    /* 8 */ "JPC",
    /* 9 */ "SIO",
};

const char *OP_2_NAMES[] = {
    /*  0 */ "RET",
    /*  1 */ "NEG",
    /*  2 */ "ADD",
    /*  3 */ "SUB",
    /*  4 */ "MUL",
    /*  5 */ "DIV",
    /*  6 */ "ODD",
    /*  7 */ "MOD",
    /*  8 */ "EQL",
    /*  9 */ "NEQ",
    /* 10 */ "LSS",
    /* 11 */ "LEQ",
    /* 12 */ "GTR",
    /* 13 */ "GEQ",
};

const char *OP_9_NAMES[] = {
    /* 0 */ "OUT",
    /* 1 */ "INP",
    /* 2 */ "HLT",
};
