// Team name: Compiler Builder 6
// Tung Cheung Leong (Beeno Tung)
// Trevor Jenkins (Snake)
// Austin VanDyne
// Joshua Yandell
// <>

#include <string.h>
#include "utils.c"
#include "token.h"
#include "parse.h"
#include "pm0code.h"

//#define debug_printf(...) printf(__VA_ARGS__)
#undef debug_printf
#define debug_printf(...) noop(__VA_ARGS__)

void noop(char *pattern, ...) {
    static bool first = true;
    if (first) {
        printf("\n");
        first = false;
    }
}

const char *ERROR_MSGs[] = {"Undefined Error.",
                            /*  1. */ "Use = instead of :=.",
                            /*  2. */ " = must be followed by a number.",
                            /*  3. */ "Identifier must be followed by = .",
                            /*  4. */ "const, var, procedure must be followed by identifier.",
                            /*  5. */ "Semicolon or comma missing.",
                            /*  6. */ "Incorrect symbol after procedure declaration.",
                            /*  7. */ "Statement expected.",
                            /*  8. */ "Incorrect symbol after statement part in block.",
                            /*  9. */ "Period expected.",
                            /* 10. */ "Semicolon between statements missing.",
                            /* 11. */ "Undeclared identifier.",
                            /* 12. */ "Assignment to constant or procedure is not allowed.",
                            /* 13. */ "Assignment operator expected.",
                            /* 14. */ "call must be followed by an identifier.",
                            /* 15. */ "Call of a constant or variable is meaningless.",
                            /* 16. */ "then expected.",
                            /* 17. */ "Semicolon or } expected.",
                            /* 18. */ "do expected.",
                            /* 19. */ "Incorrect symbol following statement.",
                            /* 20. */ "Relational operator expected.",
                            /* 21. */ "Expression must not contain a procedure identifier.",
                            /* 22. */ "Right parenthesis missing.",
                            /* 23. */ "The preceding factor cannot begin with this symbol.",
                            /* 24. */ "An expression cannot begin with this symbol.",
                            /* 25. */ "This number is too large.",
                            /* 26. */ "Invalid token (lexer bug).",
                            /* 27. */ "Undefined constant.",
                            /* 28. */ "Duplicated variable (same name).",
                            /* 29. */ "Variable name collides with an existing constant",
                            /* 30. */ "Branch \"else\" is not supported.",
                           };

//struct token_s {
//    int token_type;
//    union {
//        /* type 0 */ int intVal;
//        /* type 1 */ string strVal;
//    } value;
//    int valType; /* -1 if no value */
//};
//
//struct token_s *tok;
union tokVal_u {
    /* type 0 */ int intVal;
    /* type 1 */ string strVal;
};
int tok;
union tokVal_u tokVal;
int tokValType; /* -1 if no value */

struct code_s {
    int op;
    int l;
    int m;
};
struct code_s *codes;
int n_code = 0;

int emit(int op, int l, int m) {
    n_code++;
    codes = checked_realloc(codes, sizeof(struct code_s) * n_code, "codes (grow)");
    codes[n_code - 1].op = op;
    codes[n_code - 1].l = l;
    codes[n_code - 1].m = m;
    return n_code - 1;
}

/* used only when leaving from error state */
void print_code() {
    int i;
    printf("\ngenerated pm0 codes so far (might be incomplete):\n");
    for (i = 0; i < n_code; i++) {
        fprintf(stdout, "%d %d %d\n", codes[i].op, codes[i].l, codes[i].m);
    }
    printf("\nassembly codes (skip-lex-level-insensitive, just for debug):\n");
    for (i = 0; i < n_code; i++) {
        if (codes[i].op == 2) {
            fprintf(stdout, "%s\n", OP_2_NAMES[codes[i].m]);
        } else if (codes[i].op == 9) {
            fprintf(stdout, "%s\n", OP_9_NAMES[codes[i].m]);
        } else {
            fprintf(stdout, "%s %d %d\n", OPCODE_NAMES[codes[i].op], codes[i].l, codes[i].m);
        }
    }
}

void error(int error_code, bool skip_token) {
    print_code();
    printf("\nError number %d, %s", error_code, ERROR_MSGs[error_code]);
    if (!skip_token) {
        if (1 <= tok && tok <= 33) {
            printf(" Seen token (%d) %s\n", tok, TOKEN_NAMES[tok]);
        } else {
            printf(" Seen token (code) \"%d\"\n", tok);
        }
    }
    exit(EXIT_FAILURE);
}

struct error_args {
    int error_code;
    bool skip_token;
};

void error_wrapper(struct error_args args) {
    error(args.error_code, args.skip_token);
}

#define error(...) error_wrapper((struct error_args){__VA_ARGS__})

/* -------- begin: debug -------- */
int n_indent = 0;

void indent() {
    int i;
    for (i = 0; i < n_indent; i++) {
        debug_printf("| ");
    }
}

void enter(string name) {
    indent();
    debug_printf("begin: %s\n", name);
    n_indent++;
}

void leave(string name) {
    n_indent--;
    indent();
    debug_printf("end: %s\n", name);
}

/* -------- end: debug -------- */

/* -------- begin: symbol_table -------- */
struct int_value_s {
    string name;
    int value;
};

struct lex_level_s {
    struct int_value_s *const_val_array;
    int n_const;
    string*var_name_array;
    int n_var;
    struct lex_level_s *next;
};

struct lex_level_s *lex_level_head;

void init_symbol_table() {
    lex_level_head = checked_malloc(sizeof(struct lex_level_s), "init lex_level_s");
    lex_level_head->const_val_array = checked_malloc(sizeof(struct int_value_s), "const_val_array");
    lex_level_head->n_const = 0;
    lex_level_head->var_name_array = checked_malloc(sizeof(string), "var_name_array");
    lex_level_head->n_var = 0;
}

void create_new_level() {
    struct lex_level_s *head = checked_malloc(sizeof(struct lex_level_s), "new lex_level_s");
    head->const_val_array = checked_malloc(sizeof(struct int_value_s), "const_val_array");
    head->n_const = 0;
    head->var_name_array = checked_malloc(sizeof(string), "var_name_array");
    head->n_var = 0;
    head->next = lex_level_head;
    lex_level_head = head;
}

void leave_level() {
    //    int i;
    struct lex_level_s *head = lex_level_head;
    lex_level_head = head->next;
    //    for (i = 0; i < head->n_const; i++) {
    //        free(head->const_val_array[i]);
    //    }
    free(head->const_val_array);
    //    for (i = 0; i < head->n_var; i++) {
    //        free(head->var_name_array[i]);
    //    }
    free(head->var_name_array);
    free(head);
}

bool has_constant(string name) {
    struct lex_level_s *c;
    int i;
    for (c = lex_level_head; c != NULL; c = c->next) {
        for (i = 0; i < c->n_const; i++) {
            if (strcmp(name, c->const_val_array[i].name) == 0) {
                return true;
            }
        }
    }
    return false;
}

int get_constant(string name) {
    struct lex_level_s *c;
    int i;
    for (c = lex_level_head; c != NULL; c = c->next) {
        for (i = 0; i < c->n_const; i++) {
            if (strcmp(name, c->const_val_array[i].name) == 0) {
                return c->const_val_array[i].value;
            }
        }
    }
    error(27); //TODO
    return -1;
}

void store_constant(string name, int value) {
    int i;
    /* check local level */
    for (i = 0; i < lex_level_head->n_const; i++) {
        if (strcmp(name, lex_level_head->const_val_array[i].name) == 0) {
            error(12);
        }
    }
    /* store it */
    lex_level_head->n_const++;
    lex_level_head->const_val_array = checked_realloc(lex_level_head->const_val_array,
                                      sizeof(struct int_value_s) * lex_level_head->n_const,
                                      "grow const_val_array");
    lex_level_head->const_val_array[lex_level_head->n_const - 1].name = clone_string(name);
    lex_level_head->const_val_array[lex_level_head->n_const - 1].value = value;
}

/**
 * @return integer: position if found, -1 if not found
 * */
struct get_var_offset_s {
    int level;
    int offset;
};

struct get_var_offset_s *get_var_offset(string name) {
    struct get_var_offset_s *res = checked_malloc(sizeof(struct get_var_offset_s), "res for get_var_offset()");
    struct lex_level_s *c;
    int i;
    for (c = lex_level_head, res->level = 0; c != NULL; c = c->next, res->level++) {
        for (i = 0; i < c->n_var; i++) {
            if (strcmp(name, c->var_name_array[i]) == 0) {
                res->offset = 4 + i;
                return res;
            }
        }
    }
    res->offset = -1;
    return res;
}

void register_var(string name) {
    indent();
    debug_printf("register %s\n", name);
    int i;
    /* check local level */
    for (i = 0; i < lex_level_head->n_var; i++) {
        if (strcmp(name, lex_level_head->var_name_array[i]) == 0) {
            error(28);
        }
    }
    if (has_constant(name)) {
        error(29);
    }
    /* store it */
    lex_level_head->n_var++;
    lex_level_head->var_name_array = checked_realloc(lex_level_head->var_name_array,
                                     sizeof(string) * lex_level_head->n_var,
                                     "grow var_name_array");
    lex_level_head->var_name_array[lex_level_head->n_var - 1] = clone_string(name);
}

/* -------- end: symbol_table -------- */

const int ANY_TOKEN = 0;

void ensure_token(token_type expected_token, int error_code) {
    if (expected_token != tok) {
        print_code();
        printf("\nError number %d, %s Expect token %s but seen token %s\n", error_code, ERROR_MSGs[error_code],
               TOKEN_NAMES[expected_token],
               TOKEN_NAMES[tok]
              );
        exit(EXIT_FAILURE);
    }
}

char *token_filename;
FILE *fp_token;

void advance(token_type expected_token, int error_code) {
    //    printf("advance()\n");
    if (expected_token != ANY_TOKEN) {
        ensure_token(expected_token, error_code);
    }

    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    unsigned int len_val;
    int offset_token_type;
    bool is_string = false;
    bool is_integer = false;
    struct str_to_long_s *str_to_long_s;

    //    read = getline(&line, &len, stdin);
    read = getline(&line, &len, fp_token);
    if (read == -1) {
        tok = nulsym;
        return;
    }

    for (len_val = 0; len_val < read; len_val++) {
        if (line[len_val] == ' ') {
            break;
        }
    }
    for (offset_token_type = len_val; offset_token_type < read; offset_token_type++) {
        if (line[offset_token_type] != ' ') {
            break;
        }
    }
    str_to_long_s = str_to_long(&line[offset_token_type]);
    if (!str_to_long_s->success) {
        print_code();
        printf("\nError number %d, %s Seen token '%s'\nline(len:%ld): %s\n", 26, ERROR_MSGs[26],
               &line[offset_token_type], read, line);
        exit(EXIT_FAILURE);
    }
    tok = atoi(&line[offset_token_type]);


    switch (tok) {
    default: {
        print_code();
        printf("\nError number %d, %s Seen token '%s'\nline(len:%ld): %s\n", 26, ERROR_MSGs[26],
               &line[offset_token_type], read, line);
        exit(EXIT_FAILURE);
        break;
    }
    case nulsym:
        break;
    case identsym:
        is_string = true;
        break;
    case numbersym:
        is_integer = true;
        break;
    case plussym:
    case minussym:
    case multsym:
    case slashsym:
    case oddsym:
    case eqsym:
    case neqsym:
    case lessym:
    case leqsym:
    case gtrsym:
    case geqsym:
    case lparentsym:
    case rparentsym:
    case commasym:
    case semicolonsym:
    case periodsym:
    case becomessym:
    case beginsym:
    case endsym:
    case ifsym:
    case thensym:
    case whilesym:
    case dosym:
    case callsym:
    case constsym:
    case varsym:
    case procsym:
    case writesym:
    case readsym:
        break;
    case elsesym:
        error(30);
        break;
    }
    if (is_integer && is_string) {
        print_code();
        printf("\nError: the token value is both string and integer: seen token \"%s\"\n", line);
        exit(EXIT_FAILURE);
    } else if (is_string || is_integer) {
        tokVal.strVal = checked_malloc(sizeof(char) * len_val + 1, "token string value");
        memcpy(tokVal.strVal, line, len_val);
        tokVal.strVal[len_val] = '\0';
        tokValType = 0;
        if (is_integer) {
            tokVal.intVal = atoi(tokVal.strVal);
            tokValType = 1;
        }
    } else {
        tokValType = -1;
    }

    free(line);
    free(str_to_long_s);
    indent();
    debug_printf("read %s", TOKEN_NAMES[tok]);
    if (tok == identsym) {
        debug_printf(":%s", tokVal.strVal);
    } else if (tok == numbersym) {
        debug_printf(":%d", tokVal.intVal);
    }
    debug_printf("\n");
}

struct advance_args {
    token_type expected_token;
    int error_code;
};

void advance_wrapper(struct advance_args args) {
    advance(args.expected_token, args.error_code);
}

#define advance(...) advance_wrapper((struct advance_args){__VA_ARGS__})

/*---------------------begin of init------------------------------*/
/* declare functions */
void init() {
    noop(NULL);
    init_symbol_table();
    //    tok = checked_malloc(sizeof(struct token_s), "token_s in advance()");
    codes = checked_malloc(sizeof(struct code_s) * 0, "codes (empty array)");
    fp_token = fopen(token_filename, "r");
    if (fp_token == NULL) {
        printf("\nError: Failed to open token file: %s\n", token_filename);
        exit(EXIT_FAILURE);
    }
}

void deinit() {
    fclose(fp_token);
    fflush(stdout);
}

/*-----------------------end of init------------------------------*/
void load_ident(int error_code) {
    struct get_var_offset_s *ident = get_var_offset(tokVal.strVal);
    indent();
    debug_printf("got ident offset:%d\n", ident->offset);
    if (ident->offset == -1) {
        /*
         * this is not a registered var
         * 1. check if it's constant
         *    a. emit constant value if found
         *    b. throw error if it's not a registered constant
         * */
        if (has_constant(tokVal.strVal)) {
            emit(LIT, 0, get_constant(tokVal.strVal));
        } else {
            error(error_code);
        }
    } else { /* this is a var */
        emit(LOD, ident->level, ident->offset);
    }
    free(ident);
    advance();
}

void parse_program() {
    enter("parse_program()");
    parse_block();
    advance(periodsym, 9);
    leave("parse_program()");
}

void parse_block() {
    enter("parse_block()");
    parse_const_declaration();
    parse_var_declaration();
    parse_statement();
    leave("parse_block()");
}

void parse_const_declaration() {
    enter("parse_const_declare()");
    string name;
    if (tok == constsym) {
        advance();
        ensure_token(identsym, 4);
        name = clone_string(tokVal.strVal);
        advance(identsym);
        advance(eqsym);
        store_constant(name, tokVal.intVal);
        free(name);
        advance(numbersym);
        while (tok == commasym) {
            advance();
            name = clone_string(tokVal.strVal);
            advance(identsym);
            advance(eqsym);
            store_constant(name, tokVal.intVal);
            free(name);
            advance(numbersym);

        }
        advance(semicolonsym, 5);

    }
    leave("parse_const_declare()");
}

void parse_var_declaration() {
    enter("parse_var_declare()");
    if (tok == varsym) {
        advance();
        register_var(tokVal.strVal);
        advance(identsym);
        while (tok == commasym) {
            advance();
            register_var(tokVal.strVal);
            advance(identsym);
        }
        advance(semicolonsym, 5);
    }
    emit(INC, 0, 4 + lex_level_head->n_var);
    leave("parse_var_declare()");
}

void parse_statement() {
    enter("parse_statement()");
    if (tok == identsym) {
        struct get_var_offset_s *ident = get_var_offset(tokVal.strVal);
        if (ident->offset == -1) {
            if (has_constant(tokVal.strVal)) {
                error(12);
            } else {
                error(11);
            }
        }
        advance();
        if (tok == eqsym) {
            error(1);
        }
        advance(becomessym);
        parse_expression();
        emit(STO, ident->level, ident->offset);
        free(ident);
    } else if (tok == beginsym) {
        advance();
        parse_statement();
        while (tok == semicolonsym) {
            advance();
            parse_statement();
        }
        advance(endsym);

    } else if (tok == ifsym) {
        advance();
        parse_condition();
        advance(thensym, 16);
        int idx = emit(JPC, 0, 0);
        parse_statement();
        codes[idx].m = n_code;
    } else if (tok == whilesym) {
        advance();
        int idx1 = n_code;
        parse_condition();
        advance(dosym, 18);
        int idx2 = emit(JPC, 0, 0);
        parse_statement();
        emit(JMP, 0, idx1);
        codes[idx2].m = n_code;
    } else if (tok == readsym) {
        emit(SIO, 0, INP);
        advance();
        struct get_var_offset_s *ident = get_var_offset(tokVal.strVal);
        if (ident->offset == -1) {
            if (has_constant(tokVal.strVal)) {
                error(12);
            } else {
                error(11);
            }
        }
        emit(STO, ident->level, ident->offset);
        free(ident);
        advance(identsym);
    } else if (tok == writesym) {
        advance();
        load_ident(11);
        emit(SIO, 0, OUT);
    }
    leave("parse_statement()");
}

void parse_condition() {
    enter("parse_condition()");
    if (tok == oddsym) {
        advance();
        parse_expression();
        emit(OPR, 0, ODD);
    } else {
        parse_expression();
        parse_rel_op();
        int op_idx = n_code - 1;
        parse_expression();
        /**
         * reorder the code from A B C -> A C B
         *   where
         *     A and C are values to compare
         *     B is the compare op
         *
         * before               after
         *   codes[op_idx]        codes[...]
         *   codes[...]           codes[n_code-1]
         *   codes[n_code-1]      codes[op_idx]
         *
         * steps
         *   1. copy codes[op_idx] to temp
         *   2. shift each code [op_idx+1..n_code-1] one cell upward
         *   3. copy temp to codes[n_code-1]
         * **/
        int i;
        /* 1. */
        struct code_s code_op = codes[op_idx];
        /* 2. */
        for (i = op_idx + 1; i <= n_code - 1; i++) {
            codes[i - 1] = codes[i];
        }
        /* 3. */
        codes[n_code - 1] = code_op;
    }
    leave("parse_condition()");
}

void parse_rel_op() {
    enter("parse_rel_op()");
    switch (tok) {
    case eqsym:
        emit(OPR, 0, EQL);
        advance();
        break;
    case neqsym:
        emit(OPR, 0, NEQ);
        advance();
        break;
    case lessym:
        emit(OPR, 0, LSS);
        advance();
        break;
    case leqsym:
        emit(OPR, 0, LEQ);
        advance();
        break;
    case gtrsym:
        emit(OPR, 0, GTR);
        advance();
        break;
    case geqsym:
        emit(OPR, 0, GEQ);
        advance();
        break;
    default:
        error(20);
    }
    leave("parse_rel_op()");
}

void parse_expression() {
    bool negative = false;
    int op;
    if (tok == plussym) {
        advance();
    } else if (tok == minussym) {
        negative = true;
        advance();
    }
    parse_term();
    if (negative) {
        emit(OPR, 0, NEG);
    }
    while (tok == plussym || tok == minussym) {
        op = tok;
        advance();
        parse_term();
        if (op == plussym) {
            emit(OPR, 0, ADD);
        } else { /* op == minussym */
            emit(OPR, 0, SUB);
        }
    }
}

void parse_term() {
    int op;
    parse_factor();
    while (tok == multsym || tok == slashsym) {
        op = tok;
        advance();
        parse_factor();
        if (op == multsym) {
            emit(OPR, 0, MUL);
        } else { /* op == slashsym */
            emit(OPR, 0, DIV);
        }
    }
}

void parse_factor() {
    enter("parse_factor()");
    if (tok == identsym) {
        load_ident(11);
    } else if (tok == numbersym) {
        emit(LIT, 0, tokVal.intVal);
        advance();
    } else if (tok == lparentsym) {
        advance();
        parse_expression();
        advance(rparentsym, 22);
    } else {
        /* TODO confirm the meaning of error code 23 */
        error(23);
    }
    leave("parse_factor()");
}

int main(int argc, char **args) {
    if (argc != 1 + 2) {
        print_code();
        printf("\nError: expect two arguments\n");
        printf("example: %s tokenfile code.pm0\n", args[0]);
        exit(EXIT_FAILURE);
    }
    token_filename = args[1];

    init();

    /* vars */
    int i;
    FILE *fp_pmcode;


    /* main logic */
    //    for (tok = advance(); tok != nulsym; free(tok), tok = advance()) {
    //        printf("token:%d\n", tok);
    //        if (tokValType == 0)
    //            printf("value:%s\n", tokVal.strVal);
    //        if (tokValType == 1)
    //            printf("value:%d\n", tokVal.intVal);
    //    }
    //    printf("end with nulsym\n");

    advance();
    if (tok == nulsym) {
        printf("\nWarning: Empty File\n");
    } else {
        parse_program();
    }
    emit(SIO, 0, HLT);

    debug_printf("saving machine code...\n");
    fp_pmcode = fopen(args[2], "w");
    if (fp_pmcode == NULL) {
        print_code();
        printf("\nError: Failed to write to pmcode file: %s\n", args[2]);
        exit(EXIT_FAILURE);
    }
    for (i = 0; i < n_code; i++) {
        debug_printf("emit: %d %d %d\n", codes[i].op, codes[i].l, codes[i].m);
        fprintf(fp_pmcode, "%d %d %d\n", codes[i].op, codes[i].l, codes[i].m);
    }
    fclose(fp_pmcode);

    printf("parser finished without error.\n");

    deinit();
}
