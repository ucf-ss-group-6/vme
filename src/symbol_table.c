//
// Created by beenotung on 11/28/16.
// fork from parser.c, refer that for usage example
// TODO fix error() dep
//

#include "utils.h"
#include "compile.h"

struct int_value_s {
    string name;
    int value;
};

struct lex_level_s {
    struct int_value_s *const_val_array;
    int n_const;

    string *var_name_array;
    int n_var;

    struct int_value_s *procedure_addr_array; // use the value as address to JMP when call
    int n_procedure;

    struct lex_level_s *next;
};

struct lex_level_s *lex_level_head;

void init_symbol_table() {
    lex_level_head = checked_malloc(sizeof(struct lex_level_s), "init lex_level_s");

    lex_level_head->const_val_array = malloc(0);
    lex_level_head->n_const = 0;

    lex_level_head->var_name_array = malloc(0);
    lex_level_head->n_var = 0;

    lex_level_head->procedure_addr_array = malloc(0);
    lex_level_head->n_procedure = 0;

    lex_level_head->next = NULL;
}

void create_new_level() {
    struct lex_level_s *head = checked_malloc(sizeof(struct lex_level_s), "new lex_level_s");

    head->const_val_array = malloc(0);
    head->n_const = 0;

    head->var_name_array = malloc(0);
    head->n_var = 0;

    head->procedure_addr_array = malloc(0);
    head->n_procedure = 0;

    head->next = lex_level_head;
    lex_level_head = head;
}

/**
 * TODO test if need to free each string element manually or just free the string array
 * */
void leave_level() {
    debug_enter("leave_level()");

    //    fix_procedure_CAL();

    struct lex_level_s *head = lex_level_head;
    lex_level_head = head->next;
    int i;
    for (i = 0; i < head->n_const; i++) {
        debug_print_indent();
        debug_printf("free constant name \"%s\"\n", head->const_val_array[i].name);
        free(head->const_val_array[i].name);
    }
    for (i = 0; i < head->n_var; i++) {
        debug_print_indent();
        debug_printf("free var name \"%s\"\n", head->var_name_array[i]);
        free(head->var_name_array[i]);
    }
    for (i = 0; i < head->n_procedure; i++) {
        debug_print_indent();
        debug_printf("free procedure name \"%s\" starting at %d\n", head->procedure_addr_array[i].name,
                     head->procedure_addr_array[i].value);
        free(head->procedure_addr_array[i].name);
    }
    free(head->const_val_array);
    free(head->var_name_array);
    free(head->procedure_addr_array);
    free(head);
    debug_leave("leave_level()");
}


/* -------- begin: const -------- */

bool has_constant(string name) {
    struct lex_level_s *c;
    int i;
    for (c = lex_level_head; c != NULL; c = c->next) {
        for (i = 0; i < c->n_const; i++) {
            if (strcmp(name, c->const_val_array[i].name) == 0) {
                return true;
            }
        }
    }
    return false;
}

bool get_constant(string name, int *value) {
    struct lex_level_s *c;
    int i;
    for (c = lex_level_head; c != NULL; c = c->next) {
        for (i = 0; i < c->n_const; i++) {
            if (strcmp(name, c->const_val_array[i].name) == 0) {
                *value = c->const_val_array[i].value;
                return true;
            }
        }
    }
    push_parser_error(27);
    return false;
}

bool store_constant(string name, int value) {
    int i;
    /* check local level */
    for (i = 0; i < lex_level_head->n_const; i++) {
        if (strcmp(name, lex_level_head->const_val_array[i].name) == 0) {
            push_parser_error(12);
            return false;
        }
    }
    /* store it */
    lex_level_head->n_const++;
    lex_level_head->const_val_array = checked_realloc(lex_level_head->const_val_array,
                                      sizeof(struct int_value_s) * lex_level_head->n_const,
                                      "grow const_val_array");
    lex_level_head->const_val_array[lex_level_head->n_const - 1].name = clone_string(name);
    lex_level_head->const_val_array[lex_level_head->n_const - 1].value = value;
    return true;
}

/* -------- end: const -------- */
/* -------- begin: var -------- */

/**
 * will NOT push error
 * */
//bool get_var_offset(string name, int *level, int *offset) {
bool get_var_offset(string name, int *level_and_offset) {
    debug_enter("get_var_offset()");
    debug_printf("name = \"%s\"\n", name);
    bool res = false;
    struct lex_level_s *c;
    int i;
    for (c = lex_level_head, level_and_offset[0] = 0; c != NULL; c = c->next, level_and_offset[0]++) {
        debug_printf("enter a level\n");
        for (i = 0; i < c->n_var; i++) {
            if (strcmp(name, c->var_name_array[i]) == 0) {
                level_and_offset[1] = 4 + i;
                res = true;
                goto done;
            }
        }
    }
done:
    debug_printf("return %s: %d, %d\n", name, level_and_offset[0], level_and_offset[1]);
    debug_leave("get_var_offset()");
    return res;
}

bool register_var(string name) {
    debug_print_indent();
    debug_printf("register var %s\n", name);
    int i;
    /* check local level */
    for (i = 0; i < lex_level_head->n_var; i++) {
        if (strcmp(name, lex_level_head->var_name_array[i]) == 0) {
            push_parser_error(28);
            return false;
        }
    }
    if (has_constant(name)) {
        push_parser_error(29);
        return false;
    }
    /* store it */
    lex_level_head->n_var++;
    lex_level_head->var_name_array = checked_realloc(lex_level_head->var_name_array,
                                     sizeof(string) * lex_level_head->n_var,
                                     "grow var_name_array");
    lex_level_head->var_name_array[lex_level_head->n_var - 1] = clone_string(name);
    return true;
}

/* -------- end: var -------- */
/* -------- begin: procedure -------- */

/**
 * @param level_and_addr : [level, address]
 *   level : relative level [0, n)
 *   address : absolute address
 * */
bool get_procedure_address(string name, int *level_and_address) {
    debug_enter("get_procedure_address()");
    struct lex_level_s *c;
    int i;
    bool res = false;
    for (c = lex_level_head, level_and_address[0] = 0; c != NULL; c = c->next, level_and_address[0]++) {
        for (i = 0; i < c->n_procedure; i++) {
            if (strcmp(name, c->procedure_addr_array[i].name) == 0) {
                //                *addr = 4 + i - 1; // offset by one for the INC op
                //                *addr = c->procedure_addr_array[i].value;
                level_and_address[1] = c->procedure_addr_array[i].value;
                res = true;
                goto done;
            }
        }
    }
done:
    debug_printf("found procedure \"%s\"? %s\n", name, res ? "true" : "false");
    debug_printf("level: %d\n", level_and_address[0]);
    debug_printf("address: %d\n", level_and_address[1]);
    debug_leave("get_procedure_address()");
    return res;
}

bool register_procedure(string name, int addr) {
    debug_print_indent();
    debug_printf("register procedure %s\n", name);
    int i;
    /* check local level */
    for (i = 0; i < lex_level_head->n_procedure; i++) {
        if (strcmp(name, lex_level_head->procedure_addr_array[i].name) == 0) {
            push_parser_error(35);
            return false;
        }
    }
    /* store it */
    lex_level_head->n_procedure++;
    lex_level_head->procedure_addr_array = checked_realloc(lex_level_head->procedure_addr_array,
                                           sizeof(struct int_value_s) * lex_level_head->n_procedure,
                                           "grow procedure_addr_array");
    lex_level_head->procedure_addr_array[lex_level_head->n_procedure - 1].name = clone_string(name);
    lex_level_head->procedure_addr_array[lex_level_head->n_procedure - 1].value = addr;
    return true;
}

/* -------- end: procedure -------- */
