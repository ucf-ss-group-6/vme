// Team name: Compiler Builder 6
// Tung Cheung Leong (Beeno Tung)
// Trevor Jenkins (Snake)
// Austin VanDyne
// Joshua Yandell
// <>

#define _XOPEN_SOURCE 700

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h> // for ssize_t on ubuntu
#include <string.h>

typedef unsigned char byte;
#define bool byte
#define true 1
#define false 0
#define string char *

#define exit fflush(stdout);fflush(stderr);exit
#define printf(...) printf(__VA_ARGS__);fflush(stdout)

//#define __debug__ // comment out this line in production mode
#ifdef __debug__
#define debug_printf(...) printf(__VA_ARGS__)
#else
#define debug_printf(...) dummy_printf(__VA_ARGS__)
#endif

bool Bool(bool res) {
    return res != false;
}

void dummy_printf(char *pattern, ...) {
    static bool first = true;
    if (first) {
        first = false;
        printf("\n");
    }
}

void *checked_malloc(size_t size, const char *name) {
    void *res = malloc(size);
    if (res == NULL) {
        fprintf(stderr, "\nError: failed to alloc for %s\n", name);
        exit(EXIT_FAILURE);
    }
    return res;
}

void *checked_realloc(void *dest, size_t size, const char *name) {
    dest = realloc(dest, size);
    if (dest == NULL) {
        fprintf(stderr, "\nError: failed to realloc for %s\n", name);
        exit(EXIT_FAILURE);
    }
    return dest;
}

FILE *checked_fopen(char *filename, char *mode) {
    FILE *fp = fopen(filename, mode);
    if (fp == NULL) {
        fprintf(stderr, "\nError: Failed to open file '%s' for mode '%s'\n", filename, mode);
        exit(EXIT_FAILURE);
    }
    return fp;
}

const int MAX_IDENTIFIER_LENGTH = 12;
const int MAX_NUMBER_VALUE = 65535; /* 2^16 - 1 */

bool is_visible_char(int c) {
    return c >= 33 && c <= 126 ? true : false;
}

bool is_digit(int c) {
    return c >= '0' && c <= '9' ? true : false;
}

/**
 * @return zero / positive int : no error
 * @return -1 : not integer
 * @return -2 : excess @invariant MAX_NUMBER_VALUE
 * */
int number_value(char *cs, int n) {
    int i;
    int value = 0;
    for (i = 0; i < n; i++) {
        if (is_digit(cs[i])) {
            value = value * 10 + (cs[i] - '0');
            if (value > MAX_NUMBER_VALUE) {
                return -2;
            } else {
                continue;
            }
        } else {
            return -1;
        }
    }
    return value;
}

bool is_alphabet(int c) {
    return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') ? true : false;
}

bool is_symbol(int c) {
    return is_visible_char(c) && !is_digit(c) && !is_alphabet(c) ? true : false;
}

bool is_whitespace(int c) {
    switch (c) {
        case '\r':
        case '\n':
        case '\t':
        case '\0':
        case ' ':
        case EOF:
            return true;
        default:
            return false;
    }
}

struct str_to_long_s {
    long int value;
    bool success;
};

struct str_to_long_s *str_to_long(char *str) {
    struct str_to_long_s *res = checked_malloc(sizeof(struct str_to_long_s), "function str_to_long 's result");
    res->value = atoi(str);
    res->success = (bool) ((res->value != 0) || ((str[0] == '0') && (str[1] == '\0')));
    return res;
}

string clone_string(string input) {
    string res;
    int n;
    for (n = 0; input[n] != '\0'; n++);
    res = checked_malloc(sizeof(char) * n + 1, "res for clone_string()");
    strcpy(res, input);
    res[n] = '\0';
    return res;
}

string clone_const_string(const string input) {
    int n = strlen(input) + 1;
    string res = checked_malloc(sizeof(char) * n, "res for clone_string()");
    strcpy(res, input);
    return res;
}

bool is_str_same(const string a, const string b) {
    return strcmp(a, b) == 0;
}

/**
 * return new dest if reallocated
 * return original dest otherwise
 * */
string replace_string(string dest, string src) {
    int n = strlen(src) + 1;
    dest = checked_realloc(dest, sizeof(char) * n, "dest in replace_string()");
    strcpy(dest, src);
    return dest;
}

/**
 * return new dest if reallocated
 * return original dest otherwise
 * */
string replace_const_string(string dest, const string src) {
    int n = strlen(src) + 1;
    dest = checked_realloc(dest, sizeof(char) * n, "dest in replace_string()");
    strcpy(dest, src);
    return dest;
}

char get_last_char_in_file(char *filename) {
    FILE *fp = checked_fopen(filename, "r");
    char c1, c2;
    while (!feof(fp)) {
        c1 = c2;
        c2 = getc(fp);
    }
    fclose(fp);
    return c1;
}

/* check if input file end with newline, MODIFY the file is not */
void check_and_fix_newline(char *filename) {
    char cmd_tmp[] = "echo '' | cat %s - > tmp && mv tmp %s";
    char cmd_tmp2[] = "dos2unix %s 2>&1 | grep -v 'dos2unix: converting file'";
    char *cmd;

    cmd = checked_malloc(sizeof(char) * (
            strlen(cmd_tmp2) - 2
            + strlen(filename)
            + 1
    ), "cmd for check_and_fix_newline:dos2unix");
    sprintf(cmd, cmd_tmp2, filename);
    system(cmd);

    if (get_last_char_in_file(filename) != 10) {
        cmd = checked_realloc(cmd, sizeof(char) * (
                                      strlen(cmd_tmp) - 2 * 2
                                      + strlen(filename) * 2
                                      + 1
                              ),
                              "cmd for check_and_fix_newline:tailing newline");
        sprintf(cmd, cmd_tmp, filename, filename);
        system(cmd);
    }

    free(cmd);
}

/* ---- debug utils ---- */
int debug_n_indent = 0;

void debug_print_indent() {
#ifdef __debug__
    int i;
    for (i = 0; i < debug_n_indent; i++) {
        debug_printf("  ");
    }
#endif
}

void debug_enter(string name) {
#ifdef __debug__
    debug_print_indent();
    debug_printf("enter:%s\n", name);
#endif
    debug_n_indent++;
}

void debug_leave(string name) {
    debug_n_indent--;
#ifdef __debug__
    debug_print_indent();
    debug_printf("leave:%s\n", name);
#endif
}
