/* reduced version of vm.c */
// Team name: <>
// Tung Cheung Leong (Beeno Tung)
// <>
// <>
// <>
// <>

#define _XOPEN_SOURCE 700
#define _GNU_SOURCE
#define _POSIX_C_SOURCE 200809L

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h> // for ssize_t on ubuntu

/** @not_used */
struct IntArray {
    int *data;
    int length;
    int size;
};

typedef unsigned char byte;

struct Instruction {
    int op;
    int l;
    int m;
};

const int MAX_STACK_HEIGHT = 2000; // int array max capacity
const int MAX_CODE_LENGTH = 500; // number of lines
const int MAX_LEXI_LEVELS = 3;

const int OFFSET_FV = 0; // function return value
const int OFFSET_SL = 1; // static link
const int OFFSET_DL = 2; // dynamic link
const int OFFSET_RA = 3; // return address (next pc value)

const char *OP_CODE[] = {
    /* 00 */ "",
    /* 01 */ "LIT",
    /* 02 */ "OPR",
    /* 03 */ "LOD",
    /* 04 */ "STO", // defined by m
    /* 05 */ "CAL",
    /* 06 */ "INC",
    /* 07 */ "JMP",
    /* 08 */ "JPC",
    /* 09 */ "SIO"  // defined by m
};

const byte OP_SHOW_L[] = { // boolean array
    /* 00 */ 0,
    /* 01 */ 0,
    /* 02 */ 0,
    /* 03 */ 1,
    /* 04 */ 1,
    /* 05 */ 1,
    /* 06 */ 0,
    /* 07 */ 0,
    /* 08 */ 0,
    /* 09 */ 0,
};

const char ***OP_M_NAME; // array of array of string

void init() {
    const int n = 10;
    int i;
    if ((OP_M_NAME = malloc(n * sizeof(char **))) == NULL) {
        printf("failed to alloc OP_M_NAME\n");
        exit(EXIT_FAILURE);
    }
    for (i = 0; i < n; i++) {
        if ((OP_M_NAME[i] = malloc(sizeof(char *))) == NULL) {
            printf("\nError: failed to alloc OP_M_NAME[%d]\n", i);
            exit(EXIT_FAILURE);
        }
        //OP_M_NAME[i][0] = "";
        //OP_M_NAME[i][0] = "\0";
        OP_M_NAME[i][0] = NULL;
    }
    /* set OP name for 9 based on modif ier */
    if ((OP_M_NAME[2] = realloc(OP_M_NAME[2], 14 * sizeof(char *))) == NULL) {
        printf("\nError: failed to realloc OP_M_NAME[2]");
        exit(EXIT_FAILURE);
    }
    OP_M_NAME[2][0] = "RET";
    OP_M_NAME[2][1] = "NEG";
    OP_M_NAME[2][2] = "ADD";
    OP_M_NAME[2][3] = "SUB";
    OP_M_NAME[2][4] = "MUL";
    OP_M_NAME[2][5] = "DIV";
    OP_M_NAME[2][6] = "ODD";
    OP_M_NAME[2][7] = "MOD";
    OP_M_NAME[2][8] = "EQL";
    OP_M_NAME[2][9] = "NEQ";
    OP_M_NAME[2][10] = "LSS";
    OP_M_NAME[2][11] = "LEQ";
    OP_M_NAME[2][12] = "GTR";
    OP_M_NAME[2][13] = "GEQ";
    /* set OP name for 9 based on modif ier */
    if ((OP_M_NAME[9] = realloc(OP_M_NAME[9], 3 * sizeof(char *))) == NULL) {
        printf("\nError: failed to realloc OP_M_NAME[9]");
        exit(EXIT_FAILURE);
    }
    OP_M_NAME[9][0] = "OUT";
    OP_M_NAME[9][1] = "INP";
    OP_M_NAME[9][2] = "HLT";
};

int base(int *stack, int l, int bp) {
    while (--l >= 0) {
        bp = stack[bp] + OFFSET_DL - 1;
    }
    return bp;
}

/** @not_used */
int same_op_name(char *a, char *b) {
    return a[0] == b[0]
           && a[1] == b[1]
           && a[2] == b[2];
}

int hash_op_name(char *a) {
    return a[0] + a[1] * 10 + a[2] * 100;
}

static char *get_op_name(int op, int m) {
    if (OP_M_NAME[op][0] == '\0')
        return ((char **) OP_CODE)[op];
    else
        return ((char ***) OP_M_NAME)[op][m];
}

void print_instruction(int line, struct Instruction *ir) {
    printf("%3d  %s  ", line, get_op_name(ir->op, ir->m));
    if (OP_SHOW_L[ir->op])
        printf("%3d", ir->l);
    else
        printf("   ");
    if (ir->op == 2 || ir->op == 9)
        printf("       ");
    else
        printf("  %3d  ", ir->m);
}

void print_stack(int stack[], int bp, int sp) {
    /* declare vars */
    int i;
    int *splits;
    int i_splits = -1;
    int n_splits = 0;
    int i_bp;

    /* alloc vars */
    if ((splits = malloc(sizeof(int) * sp)) == NULL) {
        printf("\nError: failed to alloc splits of size %lu\n", sizeof(int) * sp);
        exit(EXIT_FAILURE);
    }

    /* init splits */
    //    printf("\ninit splits\n");
    for (i_bp = bp; stack[i_bp + OFFSET_DL] != 0;) {
        //        printf("\nnew split, n_splits:%d, i_bp:%d\n", n_splits + 1, i_bp);
        splits[n_splits++] = i_bp;
        i_bp = stack[i_bp + OFFSET_DL];
    }

    /* main logic */
    for (i = 1, i_splits = 0; i <= sp; i++) {
        if (i == splits[i_splits]) {
            i_splits++;
            printf("| ");
        }
        printf("%d ", stack[i]);
    }
    //    if (n_splits > 0) {
    //        printf(" \t n_splits:%d, first_split:%d", n_splits, splits[0]);
    //    }
    //    printf("\nprint_stack : sp = %d; stack_split_i = %d\n", bp, sp/*, stack_split_i*/);

    free(splits);
}

int main(int argc, char **args) {
    /* init the constant */
    init();

    /* init var */
    /*   for reading code from file   */
    FILE *fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    int n_code = 0;
    int i_code = 0;
    struct Instruction **code; // array of struct Instruction

    /* check program arguments */
    if (argc != 2) {
        printf("unexpected argument!\nexample : %s mcode.p10\n", args[0]);
        return 1;
    }

    /* prepare to read code from .pl0 file */
    //printf("reading from %s\n", args[1]);
    /* open the .pl0 code file */
    fp = fopen(args[1], "r");
    if (fp == NULL) {
        printf("\nError: failed to open file \"%s\"\n", args[1]);
        exit(EXIT_FAILURE);
    }
    /* count total number of code (line) */
    // POTENTIAL_BUG : integer overflow when the file line count is over Integer.MAX_VALUE
    while ((read = getline(&line, &len, fp)) != -1)
        n_code++;
    if (n_code > MAX_CODE_LENGTH) {
        printf("\nError: code length excess limit of %d line (currently %d line)\n", MAX_CODE_LENGTH, n_code);
        fclose(fp);
        exit(EXIT_FAILURE);
    }
    rewind(fp);
    //printf("the file has %d lines\n", n_code);
    /* read code, store in array and print translated assembler code */
    printf("PL/0 code:\n\n");
    code = malloc(n_code * sizeof(struct Instruction));
    while ((read = getline(&line, &len, fp)) != -1) {
        if ((code[i_code] = (struct Instruction *) malloc(sizeof(struct Instruction))) != NULL) {
            /* store in array */
            sscanf(line, "%d %d %d", &(code[i_code]->op), &(code[i_code]->l), &(code[i_code]->m));
            /* print translated assembler code */
            print_instruction(i_code, code[i_code]);
            printf("\n");
            if (code[i_code]->l > MAX_LEXI_LEVELS) {
                printf("\nError: failed to excess maximum lexical level of %d (currently %d)\n", MAX_LEXI_LEVELS,
                       code[i_code]->l);
                fclose(fp);
                exit(EXIT_FAILURE);
            }
        } else {
            printf("\nError: failed to alloc struct code\n");
            fclose(fp);
            exit(EXIT_FAILURE);
        }
        i_code++;
    }
    fclose(fp);
    free(line);

    exit(EXIT_SUCCESS);
}
