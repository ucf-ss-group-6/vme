#!/bin/bash
#set -e
#set -o pipefail

if [ $# -ne 2 ];then
  echo "Error: Missing arguments"
  echo "example: $0 code.pl0 code.pm0"
  exit $1
fi
plcode=$1
pmcode=$2

make bin/lexer bin/parser bin/compile && bin/compile $plcode $pmcode
exit $?
