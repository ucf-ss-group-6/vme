## Documents
 - [Extended BNF Grammar for full PL/0](parser/full-grammar.md)
 - [Error messages for the full PL/0 Parser and Lexer](../src/error.h)

## Test
### Debug method
```
make bin/compile2
valgrind -v --track-origins=yes --leak-check=full bin/compile2 $plcode $pmcode
```
### Other options:
```
#valgrind bin/compile2 $plcode $pmcode
#valgrind --leak-check=full bin/compile2 $plcode $pmcode
#valgrind --track-origins=yes --leak-check=full bin/compile2 $plcode $pmcode
#valgrind -v --track-origins=yes --leak-check=full --show-leak-kinds=all bin/compile2 $plcode $pmcode
```
### Known Issue
try_parse_ident() will trigger "Conditional jump or move depends on uninitialised value(s)" if it failed to parse ident and carry out fallback.
This is caused by ungetc() (access multiple line).
It is safe to ignore this internal behavior as it won't cause error when running without valgrind.

### To Improve
(Below is not important for this project, as PL/0 grammar is LL1)

The current parsing approach is manipulating the buffer, which is messy when fallback is needed (e.g. whitespace, block will be lose).

Maybe using an index to immutable "buffer" is better. (e.g. using fseek())

Or load all source code into an array? (might run out of memory in extreme case)
