# project-0
vm.c

## Todo List
 - [x] parse code file
 - [x] store code into mem
 - [x] translate code (op num -> op name)
 - [x] execute
    - [x] registers
    - [x] stacks
    - [x] instructions logic
 - [x] test
 - [c] pretty output format
    - [x] suggest to lecturer
    - [c] implement

[c] _stand for cancelled_

## Further features

### Optional debug mode
Currently the debug message (e.g. print stack) mess up with the std I/O for the vm
1. use flag (argument) to toggle debug mode
2. use argument to indicate debug message output channel/file

### Fetch code partially (not all in ram)
(seems this is conflicted with the requirement)
Read the first line of code, fetch from the disk every time (buffer next line?)
#### Next line (pc++)
Simply read line from the file
#### Jump forward
Still readline (need to keep track current line number of the file pointer)
#### Jump backward
Reset the file pointer to the beginning, then readline?

### Dynamically grow the stack
 - smaller footprint for simple program
 - avoid stack overflow
    - use ram if possible
    - use swap-like file if possible
    - only fail when both primary and secondary storage is used up

## Notes

### Activation Record
i = (offset + bp)

| offset | name | meaning |
|---|---|---|
| 0 | return value (FV) |
| 1 | static link (SL) |
| 2 | dynamic link (DL) |
| 3 | return address (RA) | the next ```pc``` when this function return |
