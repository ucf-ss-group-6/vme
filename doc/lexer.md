# project-1
lex.c

## Todo List
1. [x] parse the arguments
   1. [x] recognize `--source`
   2. [x] recognize `--clean`
2. [c] read whole file into an array
3. [x] replace comment content by whitespace
4. [x] replace comment token by whitespace
5. [x] handle token without whitespace in between
6. [x] tokenize the array
7. [x] print the tokens (during the tokenization process)

## Further Plan (done)
### efficient version
 - implement a next() function
   a. if the buffer is empty
      1. [x] read a byte from file
      2. do b.
   b. if the buffer is not empty
      a. if the char is not a symbol (e.g. number, alphabit)
         -> return the char
      b. if the char is a symbol
         -> 1. store the char and an extra whitespace into buffer
            2. return a whitespace
   c. if reach the EOF
      -> return '\0'
 - handle comment/code status
