program ::= block "." .
block ::= const-declaration var-declaration statement.
const-declaration ::= [ "const" ident "=" number {"," ident "="
                        number} ";"].
var-declaration ::= [ "var" ident { "," ident } ";" ] .
statement ::= [ ident ":=" expression
          | "begin" statement { ";" statement } "end"
          | "if" condition "then" statement
          | "while" condition "do" statement
          | "read" ident
          | "write" ident
          | ε ] .
condition ::= "odd" expression
          | expression rel-op expression.
rel-op ::= "=" | "<>" | "<" | "<=" |">" | ">=".
expression ::= [ "+" | "-" ] term { ("+" | "-") term } .
term ::= factor { ( "*" | "/" ) factor } .
factor ::= ident | number | "(" expression ")" .
number ::= digit {digit}.
ident ::= letter {letter | digit}.
digit ::= "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" |
          "8" | "9" .
letter ::= "a" | "b" | ... | "y" | "z" |
           "A" | "B" | ... | "Y" | "Z".
