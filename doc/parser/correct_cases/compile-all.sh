#!/usr/bin/env bash
cd ../../../
make
cd doc/parser/correct_cases
mv ../../../compile ./
mv ../../../bin ./
./compile correct0.pl0 correct0.pm0 > correct0.log
./compile correct1.pl0 correct1.pm0 > correct1.log
./compile correct2.pl0 correct2.pm0 > correct2.log
./compile correct3.pl0 correct3.pm0 > correct3.log
./compile correct4.pl0 correct4.pm0 > correct4.log
