#!/bin/bash
set -e
set -o pipefail

#plcode='submodule/extra-test-case/res/parser/fib.pl0'
#pmcode='fib.pm0'
#plcode='submodule/extra-test-case/res/lexer/basic.pl0'
#pmcode='basic.pm0'
plcode='doc/full-compiler/demo.pl0'
pmcode='demo.m'

echo "compiling"
make bin/compile2 bin/vm bin/printer

echo "running parser"
#bin/compile2 $plcode $pmcode
#valgrind bin/compile2 $plcode $pmcode
#valgrind --leak-check=full bin/compile2 $plcode $pmcode
#valgrind --track-origins=yes --leak-check=full bin/compile2 $plcode $pmcode
valgrind -v --track-origins=yes --leak-check=full bin/compile2 $plcode $pmcode
#valgrind -v --track-origins=yes --leak-check=full --show-leak-kinds=all bin/compile2 $plcode $pmcode

#echo "printing generated code"
#bin/printer $pmcode | tee "$pmcode.txt"

echo "running vm"
bin/vm $pmcode | tee log
#submodule/course_repo/0_virtual_machine/a_project_supporting_files/vm $pmcode | tee log
#submodule/course_repo/3_compile_full/vm $pmcode | tee log
