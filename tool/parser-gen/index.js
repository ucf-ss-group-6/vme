const fs = require('fs');
let dot = (a, b, context)=>function () {
    let res = a.apply(context, arguments);
    b.call(context, res);
};
let last = xs=>xs[xs.length - 1];
let is_wrap_by = (s, a, b)=>s[0] == a && last(s) == b;
let unwrap = s=>s.slice(1, s.length - 1).trim();
let is_optional = s=> {
    s = s.trim();
    return s[0] == '[' && last(s) == ']';
};
let is_star = s => {
    s = s.trim();
    return s[0] == '{' && last(s) == '}';
};
let is_terminal = s=> {
    s = s.trim();
    return is_wrap_by(s, '"', '"') && s.split(' ').length == 1;
};
function Grammar() {
}
Grammar.prototype.toString = function () {
    return this.name;
};
function tokenize(s) {
    s = unwrap(s);
    switch (s) {
        case '.':
            return 'periodsym';
        case ',':
            return 'commasym';
        case '=':
            return 'eqsym';
        case ';':
            return 'semicolonsym';
        default:
            return s.toLowerCase() + 'sym';
    }
}
let replace_all = (s, a, b)=> {
    while (s.includes(a)) {
        s = s.replace(a, b);
    }
    return s;
};
function escape_name(s) {
    s = replace_all(s, '-', '_');
    return s;
}
let func_name = s=>'parse_' + escape_name(s);
function extra_wrap(s, start, end) {
    s = s.trim();
    if (s.indexOf(start) == 0 && s.includes(end)) {
        if (s.indexOf(end) == s.lastIndexOf(end)) {
            let body = s.slice(1, s.indexOf(end));
            let res = s.slice(s.indexOf(end) + 1);
            return [[body.trim(), res.trim()]];
        } else {
            throw new Error('not impl 3');
        }
    } else {
        return [];
    }
}
function format_ident(s, ident) {
    return s
    // .split(';').join(';\n')
    // .split('}').join('}\n')
    // .split('\n').map(line=>ident + line+'\n').join('');
        .split(';').join(';\n' + ident)
        // .split('}').join('}\n')
        ;
}
function gen_code(s, name, ident) {
    console.log(arguments);
    if (name == 'expression')
        debugger;
    ident = ident || '';
    s = s.trim();
    if (s[0] == '|') {
        s = s.slice(1);
    }
    if (s == '' || s == null || s == void 0) {
        return '';
    } else if (is_terminal(s)) {
        return ident + `advance(${tokenize(s)});`
    } else if (is_func_name(s)) {
        return ident + func_name(s) + '();'
    } else if (s[0] == '[') {
        if (is_optional(s)) {
            s = unwrap(s);
            let xs = s.split(' ').map(x=>x.trim());
            let p = '';
            if (is_terminal(xs[0])) {
                p = 'tok==' + tokenize(xs[0]);
            } else if (is_func_name(xs[0])) {
                p = func_name(xs[0]) + '()';
            } else {
                debugger;
                console.log({s: s, 'xs[0]': xs[0], xs: xs});
                throw new Error('not impl 5')
            }
            let b = ident + gen_code(xs.slice(1).join(' '), name, ident + '  ');
            return `if(${p}){
${b}
}`.split('\n').map(x=>ident + x + '\n').join('');
        }
        let res = extra_wrap(s, '[', ']');
        let [[body,tail]]=res;
        body = '[ ' + body + ' ]';
        return ident + gen_code(body, name, ident + '  ') + '\n' + ident + gen_code(tail, name, ident + '  ');
    } else if (s[0] == '{') {
        if (is_star(s)) {
            s = unwrap(s);
            let xs = s.split(' ').map(x=>x.trim());
            let p = '';
            if (is_terminal(xs[0])) {
                p = 'tok==' + tokenize(xs[0]);
            } else if (is_func_name(xs[0])) {
                p = func_name(xs[0]) + '()';
            } else {
                console.log({s: s});
                throw new Error('not impl 6')
            }
            let b = ident + gen_code(xs.slice(1).join(' '), name, ident + '  ');
            return `while(${p}){
${b}
}`.split('\n').map(x=>ident + x + '\n').join('');
        }
        let res = extra_wrap(s, '{', '}');
        if (res.length != 1) {
            debugger;
            console.log({res: res, s: s});
            throw new Error('not impl 8');
        }
        let [[body,tail]]=res;
        body = '{ ' + body + ' }';
        return ident + gen_code(body, name, ident + '  ') + '\n' + ident + gen_code(tail, name, ident + '  ');
    } else if (s.includes('{')) {
        let a = s.slice(0, s.indexOf('{'));
        let b = s.slice(s.indexOf('{'), s.indexOf('}') + 1);
        let c = s.slice(s.indexOf('}') + 1);
        return [a, b, c].map(s=>ident + gen_code(s, name, ident + '  ')).join('');
    } else if (s.includes('|')) {
        let xs = s.split('|').map(x=>x.trim());
        let p = xs.map(s=> {
            if (is_terminal(s)) {
                return 'tok==' + tokenize(s);
            } else if (is_func_name(s)) {
                return func_name(s) + '()'
            } else {
                // let xs = s.split(' ');
                // return xs.map(x=>gen_code(x, name, ident + '  '));
                // console.log({s: s});
                // throw new Error('not impl 7')
                return gen_code(s, name, ident + '  ');
            }
        }).reduce((acc, c)=> {
            return acc + ' || ' + c
        });
        return `if (${p}){
}else{
  error();
}`.split('\n').map(x=>ident + x + '\n').join('');
    } else if (s == 'ε') {
        return 'true'
    } else {
        let xs = s.split(' ').map(x=>x.trim());
        return ident + gen_code(xs[0], name, ident + '  ') + ident + gen_code(xs.slice(1).join(' '), name, ident + '  ');
    }
}
function gen_code_old(s, name, ident) {
    ident = ident || '';
    s = s.trim();
    if (is_optional(s)) {
        s = unwrap(s).trim();
        let xs = s.split(' ').map(x=>x.trim());
        let a = xs[0];
        let b = xs.slice(1).join(' ');
        let condition = '';
        if (is_terminal(a)) {
            condition = `tok==${tokenize(a)}`;
        } else if (is_func_name(a)) {
            condition = escape_name(a) + '()';
        } else {
            console.error(s);
            throw new Error('not impl 1');
        }
        let body = gen_code(b, name, ident + '  ');
        // body = format_ident(body, ident);
        return ident + `if(${condition}){
  advance();
${body}
}/* end if (${condition}) */\n`;
    } else if (is_star(s)) {
        s = unwrap(s).trim();
        let xs = s.split(' ').map(x=>x.trim());
        let a = xs[0];
        let b = xs.slice(1).join(' ');
        let condition = '';
        if (is_terminal(a)) {
            condition = `tok==${tokenize(a)}`;
        } else if (names().includes(a)) {
            condition = escape_name(a) + '()';
        } else {
            console.error(s);
            throw new Error('not impl 1');
        }
        let body = gen_code(b, name, ident);
        return ident + `while(${condition}){
  advance();
${body}
} /* end while(${condition}) */\n`
    } else if (is_terminal(s)) {
        return ident + `advance(${tokenize(s)});`
    } else if (is_func_name(s) && s != name) {
        return ident + escape_name(s) + '();';
    } else if (s == '|') {
        return ' || ';
    } else if (s == 'ε') {
        return 'true';
    } else {
        if (s == '')
            return '';
        console.log({checking: s, name: name});
        let star_res = extra_wrap(s, '{', '}');
        if (star_res.length == 1) {
            let [[body,res]]=star_res;
            // console.log({body:body,res:res});
            return ident + gen_code('{' + body + '}', name + ident) + gen_code(res, name + ident);
        } else {
            let option_res = extra_wrap(s, '[', ']');
            if (option_res.length == 1) {
                let [[body,res]]=option_res;
                return ident + gen_code('[' + body + ']', name, ident) + gen_code(res, name, ident);
            } else {
                if (s.includes('|')) {
                    let xs = s.split('|').map(x=>x.trim());
                    let a = xs[0];
                    let b = xs.slice(1).join(' | ');
                    let p1 = '';
                    if (is_terminal(a)) {
                        p1 = `tok==${tokenize(a)}`;
                    } else {
                        p1 = escape_name(a) + '()';
                    }
                    let body = gen_code(b, name, ident)
                        .split('\n').map(line=>ident + line + '\n').join('');
                    return `if(${p1}){
}else{
${body}
}`.split('\n').map(line=>ident + line).join('\n');
                } else {
                    // console.error(s);
                    // throw new Error('not impl 4')
                }
            }
        }
        let xs = s.split(' ').map(x=>x.trim());
        if (xs.length == 1) {
            // console.error(s);
            // console.log({xs:xs});
            // console.log('names',names());
            // throw new Error('not impl 2');
            return gen_code(s, name, ident);
        } else {
            let a = xs[0];
            let b = xs.slice(1).join(' ');
            return ident + gen_code(a, name, ident) + gen_code(b, name + ident);
        }
    }
}
let codes = {};
let names = ()=>Object.keys(codes);
let is_func_name = s=> {
    // console.log({s:s,names:names()});
    return names().includes(s)
};
fs.readFile('input.md', 'utf-8', (err, data)=> {
    if (err) {
        console.error(err);
    } else {
        let lines = data.split('\n').map(x=>x.trim());
        let grammars = [];
        let grammar = "";
        lines.forEach(line=> {
            if (last(line) == '.') {
                grammars.push(grammar + ' ' + line);
                grammar = "";
            } else {
                grammar += ' ' + line;
            }
        });
        grammars.forEach(line=> {
            let xs = line.split("::=").map(x=>x.trim());
            let name = xs[0];
            let body = xs[1];

            if (last(body) == '.') {
                body = body.slice(0, body.length - 1).trim();
            } else {
                console.warn('expect the EBNF statement end by a period symbol');
            }

            codes[name] = body;
        });
        names().forEach(name=> {
            let statement = codes[name];
            let code = gen_code(codes[name], name);
            codes[name] = `bool ${func_name(name)}(){
  ${format_ident(code, '  ')}}`;
            // console.log({name: name, statement: statement, code: codes[name]});
            console.log('----\nname:');
            console.log(name);
            console.log('----\nstatement:');
            console.log(statement);
            console.log('----\ncode:');
            console.log(codes[name]);
            console.log();
        });
        console.log(codes);
        names().forEach(name=> {
            let res = {};
            res[name] = codes[name];
            console.log('------------------------------------------');
            console.log(res);
        });
        let output = names().map(name=>codes[name]).reduce((acc, c)=>acc + '\n' + c);
        // console.log({output:output});
        fs.writeFile('output.c', output, err=>console.error(err));
    }
});
