
void parse_program() {
    parse_block();
    advance(periodsym);
}

void parse_block() {
    parse_const_declaration();
    parse_var_declaration();
    parse_statement();
}

void parse_const_declaration() {
    if (tok->token_type == constsym) {
        parse_ident();
        advance(eqsym);
        parse_number();
        while (tok->token_type == commasym) {
            parse_ident();
            advance(eqsym);
            parse_number();

        }
        advance(semicolonsym);

    }
}

void parse_var_declaration() {
    if (tok->token_type == varsym) {
        parse_ident();
        while (tok->token_type == commasym) {
            parse_ident();

        }
        advance(semicolonsym);

    }
}

void parse_statement() {
    if (tok->token_type == identsym) {
        advance();
        advance(becomessym);
        parse_expression();

    } else if (tok->token_type == beginsym) {
        advance();
        parse_statement();
        while (tok->token_type == semicolonsym) {
            advance();
            parse_statement();
        }
        advance(endsym);

    } else if (tok->token_type == ifsym) {
        advance();
        parse_condition();
        advance(thensym);
        parse_statement();

    } else if (tok->token_type == whilesym) {
        advance();
        parse_condition();
        advance(dosym);
        parse_statement();

    } else if (tok->token_type == readsym) {
        advance();
        advance(identsym);

    } else if (tok->token_type == writesym) {
        advance();
        advance(identsym);

    }
}

void parse_condition() {
    if (tok->token_type == oddsym) {
        advance();
        parse_expression();

    } else {
        parse_expression();
        parse_rel_op();
        parse_expression();

    }
}

void parse_rel_op() {
    switch (tok) {
        case eqsym:
            break;
        case neqsym:
            break;
        case lessym:
            break;
        case leqsym:
            break;
        case gtrsym:
            break;
        case geqsym:
            break;
        default:
            error();
    }
}

void parse_expression() {
    if (tok->token_type == plussym) {

    } else if (tok->token_type == minussym) {

    }
    parse_term();
    while (tok->token_type == plussym || tok->token_type == minussym) {
        advance();
        parse_term();
    }
}

void parse_term() {
    parse_factor();
    while (tok->token_type == multsym || tok->token_type == slashsym) {
        advance();
        parse_factor();
    }
}

void parse_factor() {
    if (tok->token_type == identsym) {
        advance();

    } else if (tok->token_type == numbersym) {
        advance();

    } else if (tok->token_type == lparentsym) {
        advance();
        parse_expression();
        advance(rparentsym);
    } else {
        error();
    }
}
