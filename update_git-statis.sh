#!/usr/bin/env bash
sed -i '/^### line of changes$/,+99d' git-statis.md
echo '### line of changes' >> git-statis.md
echo '| number of line changed | commiter |' >> git-statis.md
echo '|---|---|' >> git-statis.md
git ls-files | grep -v submodule | grep -v '^git-statis\.md$' | xargs -n1 git blame --line-porcelain | sed -n 's/^author //p' | sort | uniq -c | sed -r 's/\s+/ /g' | sed 's/ /|/' | sed 's/ /|/' | sed 's/$/|/' | sed 's/|/ | /g' | cut -c 2- | rev | cut -c 2- | rev >> git-statis.md
sed -i '/Spoupovitch/d' git-statis.md
