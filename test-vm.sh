#!/bin/bash
res=$(bash format.sh src/vm.c && bash build.sh)
res="$?"
if [ "$res" -ne 0 ]; then
  echo "Build Failed."
  exit "$res"
else
  # build success
  bin/vm submodule/course_repo/0_virtual_machine/a_project_supporting_files/mcode.pl0               > trace.txt
  bin/vm submodule/course_repo/0_virtual_machine/a_project_supporting_files/test-cases/add.pm0      > trace-add.txt
  bin/vm submodule/course_repo/0_virtual_machine/a_project_supporting_files/test-cases/iosample.pm0 < submodule/course_repo/0_virtual_machine/a_project_supporting_files/test-cases/input-iosample.txt > trace-iosample.txt
  bin/vm submodule/course_repo/0_virtual_machine/a_project_supporting_files/test-cases/swap.pm0     > trace-swap.txt
fi

echo "checking against the given trace"
echo 'check results:' > "$0.trace-diff.tmp"

name="trace.txt"
dos2unix -q "$name"
echo "$name" >> "$0.trace-diff.tmp"
diff -d --minimal "$name" "submodule/course_repo/0_virtual_machine/a_project_supporting_files/$name" >> "$0.trace-diff.tmp"

name="trace-add.txt"
dos2unix -q "$name"
echo "$name" >> "$0.trace-diff.tmp"
diff -d --minimal "$name" "submodule/course_repo/0_virtual_machine/a_project_supporting_files/test-cases/$name" >> "$0.trace-diff.tmp"

name="trace-iosample.txt"
dos2unix -q "$name"
echo "$name" >> "$0.trace-diff.tmp"
diff -d --minimal "$name" "submodule/course_repo/0_virtual_machine/a_project_supporting_files/test-cases/$name" >> "$0.trace-diff.tmp"

name="trace-swap.txt"
dos2unix -q "$name"
echo "$name" >> "$0.trace-diff.tmp"
diff -d --minimal "$name" "submodule/course_repo/0_virtual_machine/a_project_supporting_files/test-cases/$name" >> "$0.trace-diff.tmp"

cat "$0.trace-diff.tmp" | grep -v '^>' | grep -v '^<' | grep -v '^---$'
rm "$0.trace-diff.tmp"
