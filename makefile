CC = gcc
CFLAGS = -g -Wall

EXECUTABLES = \
  bin/vm \
  bin/lexer \
  bin/parser \
  bin/compile-tiny \
  bin/printer \
  bin/scanner \
  bin/compile-full \
  compile

all: $(EXECUTABLES)
	echo 'make all done'

bin:
	mkdir -p bin

clean:
	rm -rf bin


# vm
bin/vm: bin src/vm.c
	$(CC) $(CFLAGS) -o bin/vm src/vm.c

# lexer
bin/lexer: bin src/lexer.c
	$(CC) $(CFLAGS) -o bin/lexer src/lexer.c

# utils
src/utils.h: src/utils.c
	echo please makeheaders src/utils.c !!!

# parser
bin/parser: bin src/parser.c src/utils.c src/token.h src/parse.h src/pm0code.h
	$(CC) $(CFLAGS) -o bin/parser src/parser.c

# compile-tiny
bin/compile-tiny: bin src/compile-tiny.c src/utils.c
	$(CC) $(CFLAGS) -o bin/compile-tiny src/compile-tiny.c

#compile: bin/compile-tiny bin/lexer bin/parser
#	cp bin/compile-tiny compile
compile: bin/compile-full
	cp bin/compile-full compile

bin/printer: bin src/printer.c
	$(CC) $(CFLAGS) -o bin/printer src/printer.c

bin/scanner: bin src/scanner.c
	$(CC) $(CFLAGS) -o bin/scanner src/scanner.c

# compile
src/compile.h: src/compile.c
	echo please makeheaders src/compile.c !!!

bin/compile-full: bin src/compile.c src/utils.c src/compile.h src/pm0code.h src/symbol_table.c
	$(CC) $(CFLAGS) -o bin/compile-full src/compile.c
