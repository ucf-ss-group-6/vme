#!/bin/bash
set -e
set -o pipefail

if [ $# -eq 0 ]; then
  echo "expect at least one filename in arg";
  exit 1;
fi

## add space after comma, if, for, while
### non-target: those pattern in string
sed -i 's/,/, /g' $@
sed -i 's/", "/","/g' $@
sed -i "s/', '/','/g" $@
sed -i 's/,  /, /g' $@
sed -i 's/if(/if (/g' $@
sed -i 's/if  (/if (/g' $@
sed -i 's/for(/for (/g' $@
sed -i 's/while(/while (/g' $@
sed -i 's/while  (/while (/g' $@

## remove space in circle bracket
sed -i 's/( /(/g' $@
sed -i 's/ )/)/g' $@

## wrap assignment sign with space
### target: =
### non-target: ==, >=, <=, !=, +=, -=, *=, /=, %=, :=, &=, |=
### non-target: those pattern in string
sed -i 's/=/ = /g' $@
sed -i 's/" = "/"="/g' $@
sed -i "s/' = '/'='/g" $@
sed -i 's/  = / = /g' $@
sed -i 's/  = / = /g' $@
sed -i 's/ =  / = /g' $@
sed -i 's/ =  / = /g' $@
sed -i 's/= =/==/g' $@
sed -i 's/> = />=/g' $@
sed -i 's/< = /<=/g' $@
sed -i 's/! = /!=/g' $@
sed -i 's/+ = /+=/g' $@
sed -i 's/- = /-=/g' $@
sed -i 's/\* = /*=/g' $@
sed -i 's/\/ = /\/=/g' $@
sed -i 's/% = /%=/g' $@
sed -i 's/: = /:=/g' $@
sed -i 's/& = /\&=/g' $@
sed -i 's/| = /|=/g' $@

## wrap compare logic and calc & assign sign with space
### target: !=, >=, <=, ==, +=, -=, *=, /=, %=, &=, |=
### non-target: those pattern in string
sed -i 's/!=/ != /g' $@
sed -i 's/  != / != /g' $@
sed -i 's/ !=  / != /g' $@
sed -i 's/>=/ >= /g' $@
sed -i 's/" >= "/">="/g' $@
sed -i 's/  >= / >= /g' $@
sed -i 's/ >=  / >= /g' $@
sed -i 's/<=/ <= /g' $@
sed -i 's/" <= "/"<="/g' $@
sed -i 's/  <= / <= /g' $@
sed -i 's/ <=  / <= /g' $@
sed -i 's/==/ == /g' $@
sed -i 's/  == / == /g' $@
sed -i 's/ ==  / == /g' $@
sed -i 's/+=/ += /g' $@
sed -i 's/  += / += /g' $@
sed -i 's/ +=  / += /g' $@
sed -i 's/-=/ -= /g' $@
sed -i 's/  -= / -= /g' $@
sed -i 's/ -=  / -= /g' $@
sed -i 's/\*=/ *= /g' $@
sed -i 's/  \*= / *= /g' $@
sed -i 's/ \*=  / *= /g' $@
sed -i 's/\/=/ \/= /g' $@
sed -i 's/  \/= / \/= /g' $@
sed -i 's/ \/=  / \/= /g' $@
sed -i 's/%=/ %= /g' $@
sed -i 's/  %= / %= /g' $@
sed -i 's/ %=  / %= /g' $@
sed -i 's/&=/ \&= /g' $@
sed -i 's/  &= / \&= /g' $@
sed -i 's/ &=  / \&= /g' $@
sed -i 's/|=/ |= /g' $@
sed -i 's/  |= / |= /g' $@
sed -i 's/ |=  / |= /g' $@

## default formatting
## astyle documentation : http://astyle.sourceforge.net/astyle.html
res=$(astyle -s4 --mode=c -Y $@)
exit $?
