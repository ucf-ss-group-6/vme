#!/bin/bash

## setup astyle
name="astyle.zip"
url="http://pilotfiber.dl.sourceforge.net/project/astyle/astyle/astyle%202.05.1/AStyle_2.05.1_windows.zip"
echo "downloading from $url"
mkdir -p /tmp/dl
curl -o "/tmp/dl/$name" "$url"
echo "extracting $name ..."
cd /tmp/dl
rm -rf "AStyle"
unzip "$name"
echo "moving to $HOME/local/bin/astyle"
mkdir -p "$HOME/local/bin"
mv "AStyle/bin/AStyle.exe" "$HOME/local/bin/astyle.exe"
echo 'export PATH=$PATH:$HOME/local/bin' >> ~/.bashrc
echo "finished setup astyle"
echo 'please restart the ssh session or run `source ~/.bashrc`'
