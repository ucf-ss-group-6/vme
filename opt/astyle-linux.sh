#!/bin/bash

## setup astyle
url="http://heanet.dl.sourceforge.net/project/astyle/astyle/astyle%202.05.1/astyle_2.05.1_linux.tar.gz"
echo "downloading from $url"
tmpdir="/tmp/$USER/$0/$RANDOM.dl"
mkdir -p "$tmpdir"
cd "$tmpdir"
curl -o astyle.tar.gz "$url"
echo "extracting astyle.tar.gz..."
tar xzf astyle.tar.gz
cd astyle/build/gcc
echo "building astyle..."
make
echo "moving to ~/local/opt/astyle"
rm -rf ~/local/opt/astyle
mkdir -p ~/local/opt/astyle
mv bin ~/local/opt/astyle
mv obj ~/local/opt/astyle
mkdir -p ~/local/bin
echo '#!/bin/bash
~/local/opt/astyle/bin/astyle $@' > ~/local/bin/astyle
chmod +x ~/local/bin/astyle
echo 'export PATH=$PATH:$HOME/local/bin' >> ~/.bashrc
echo "finished setup astyle"
echo 'please restart the ssh session or run `source ~/.bashrc`'
