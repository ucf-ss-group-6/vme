# COP3402-16Fall 0001 Systems Software
repo for all projects for the course

## Official Information
 - [course repo](https://bitbucket.org/schneider128k/2016_fall_2016_cop3402.git)
 - [course repo wiki](https://bitbucket.org/schneider128k/2016_fall_2016_cop3402/wiki)

### project-0 (PM/0 virtual machine)
 - [vm](doc/vm.md)

### project-1 (PL/0 lexer)
 - [lexer](doc/lexer.md)
 - [collaboration](doc/lexer-collaboration.txt)

### project-2 (Tiny PL/0 Parser and Code Generator)
 - [Spec](http://www.cs.ucf.edu/~wocjan/Teaching/2016_Fall/cop3402/2_homeworks/IntermediateCodeGeneration.pdf)
 - [tiny parser](doc/tiny-parser.md)
 - [collaboration](doc/tiny-parser-collaboration.txt)

### project-3 (PL/0 Compiler)
 - [Spec](http://www.cs.ucf.edu/~wocjan/Teaching/2016_Fall/cop3402/2_homeworks/FullCodeGeneration.pdf)
 - [full compiler](doc/full-compiler.md)
 - [collaboration](doc/full-compiler-collaboration.txt)

### general collaboration
 - [git-statis](git-statis.md)
