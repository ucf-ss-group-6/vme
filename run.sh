#!/bin/bash
#set -e
#set -o pipefail

if [ $# -ne 1 ];then
  echo "Error: Missing arguments"
  echo "example: $0 code.pm0"
  exit $1
fi
pmcode=$1

make bin/vm && bin/vm $pmcode
exit $?
