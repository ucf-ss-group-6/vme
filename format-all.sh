#!/bin/bash
set -e
set -o pipefail

bash format.sh src/vm.c
bash format.sh src/lexer.c
bash format.sh src/parser.c
bash format.sh src/compile-tiny.c
bash format.sh src/scanner.c
bash format.sh src/compile.c
bash format.sh src/symbol_table.c

exit $?
